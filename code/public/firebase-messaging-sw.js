// eslint-disable-next-line no-undef
// eslint-disable-next-line no-undef
importScripts(
  "https://www.gstatic.com/firebasejs/9.2.0/firebase-app-compat.js"
);
importScripts(
  "https://www.gstatic.com/firebasejs/9.2.0/firebase-messaging-compat.js"
);

if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("../firebase-messaging-sw.js")
    .then(function (registration) {
      console.log("Registration successful, scope is:", registration.scope);
    })
    .catch(function (err) {
      console.log("Service worker registration failed, error:", err);
    });
}
// Initialize the Firebase app in the service worker by passing the generated config
const firebaseConfig = {
  apiKey: "AIzaSyDU2edSdegoFs11a_DocHIp8oY9h0pFPv0",
  authDomain: "mwstore-notification.firebaseapp.com",
  databaseURL: "https://mwstore-notification-default-rtdb.firebaseio.com",
  projectId: "mwstore-notification",
  storageBucket: "mwstore-notification.appspot.com",
  messagingSenderId: "508025420900",
  appId: "1:508025420900:web:ae088a50c2a0b029307371",
  measurementId: "G-MMTX4705BM",
};

// eslint-disable-next-line no-undef
firebase.initializeApp(firebaseConfig);

// Retrieve firebase messaging
// eslint-disable-next-line no-undef
const messaging = firebase.messaging();

self.addEventListener("notificationclick", function (event) {
  console.log("Event ", event);
  event.notification.close();
  event.waitUntil(self.clients.openWindow(event.notification.data.url));
});

messaging.onBackgroundMessage(function (payload) {
  console.log("Handling background message ", payload);

  return self.registration.showNotification(payload.notification.title, {
    body: payload.notification.body,
    icon: "./firebase-logo.png",
    tag: payload.notification.tag,
    data: payload.data,
    image: payload.notification.image,
    actions: payload.notification.actions,
  });
});
