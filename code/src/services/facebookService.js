import otherApi from "~/apis/otherApi";

export const initFacebookSDK = () => {
  if (window.FB) {
    window.FB.XFBML.parse();
  }

  let locale = "en_US";
  window.fbAsyncInit = function () {
    window.FB.init({
      appId: 1044260896507355,
      cookie: true, // enable cookies to allow the server to access
      // the session
      xfbml: true, // parse social plugins on this page
      version: "v2.5", // use version 2.1
    });
  };
  // Load the SDK asynchronously
  (function (d, s, id) {
    var js,
      fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = `//connect.facebook.net/${locale}/sdk.js`;
    fjs.parentNode.insertBefore(js, fjs);
  })(document, "script", "facebook-jssdk");

  if (window.ZaloSocialSDK) {
    window.ZaloSocialSDK.reload();
  }
};

export const cacheFacebook = (url) => {
  otherApi
    .cacheFacebook(url)
    .then((res) => console.log("Cache success"))
    .catch((err) => console.log("Cache fail"));
};
