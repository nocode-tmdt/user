import React, { useState } from "react";
import { Modal, Space } from "antd";
import className from "classnames/bind";
import useToggle from "~/hooks/useToggle";
import notificationApi from "~/apis/notificationApi";
import toast from "~/helpers/toast";
import {
  requestPermission,
  onMessageListener,
} from "~/services/firebase";
import styles from "~/themes/Notification/Notification.module.scss";

const cx = className.bind(styles);

function Notification() {
  const [openNoti, toggleNoti] = useToggle(false);
  const [confirmLoading, setConfirmLoading] = useState(false);

  const handleOk = async () => {
    setConfirmLoading(true);
    try {
      const token = await requestPermission();
      // console.log("Token: ", token);
      await notificationApi.saveTokenNotification({ token });
      setConfirmLoading(false);
      toggleNoti();
    } catch (error) {
      console.log(error);
      setConfirmLoading(false);
      toggleNoti();
      toast.warning("Thất bại", "Đăng ký thông báo thật bại !");
    }
  };

  onMessageListener()
    .then((payload) => {
      const { title, body, icon, image, actions } = payload.notification;
      const options = { title, body, icon, image, actions };
      console.log(title, options);
      toast.success(title, body);
    })
    .catch((err) => console.log("Error: ", err));

  return (
    <>
      <div className={cx("btn-noti")} onClick={toggleNoti}>
        <i className="fas fa-bell"></i>
      </div>

      <Modal
        title="Thông báo"
        bodyStyle={{ padding: "8px" }}
        visible={openNoti}
        onCancel={toggleNoti}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        closable={false}
        okText="Có"
        cancelText="Không"
      >
        <Space
          direction="horizontal"
          align="center"
          style={{ width: "100%", justifyContent: "center" }}
        >
          <p className={cx("text-noti")}>Bạn có muốn đăng ký nhận thông báo ?</p>
        </Space>
      </Modal>
    </>
  );
}

export default Notification;
