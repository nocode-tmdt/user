import React from "react";
import { Link, useRouteMatch } from "react-router-dom";

function CustomLinkMobile(props) {
  const { to, name, icon, exact } = props;
  const match = useRouteMatch({ path: to, exact: exact });

  return (
    <Link
      to={to}
      className={`navbar-side-item ${match && to !== "" ? "active" : ""}`}
    >
      <span className="navbar-side-icon">{icon}</span>
      <div className="navbar-side-link">{name}</div>
    </Link>
  );
}

export default CustomLinkMobile;
