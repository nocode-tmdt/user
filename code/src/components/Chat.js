import React, { useEffect, useState, useRef } from "react";
import { useSelector } from "react-redux";
import io from "socket.io-client";
import clsx from "clsx";
import useToggle from "~/hooks/useToggle";
import messageApi from "~/apis/messageApi";
import config from "~/constants";
import adminImg from "~/assets/img/avatar/admin.png";

const socket = io(config.api.BASE_URL_NODE);

function Chat() {
  const user = useSelector((state) => state.userReducer.user);
  const [chat, setChat] = useState("");
  const [messages, setMessages] = useState([]);
  const [openChat, toggleChat] = useToggle(false);
  const [loadMessage, setLoadMessage] = useState(true);
  const messagesEndRef = useRef(null);

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth", block: "end" });
  };

  const onMouseEnter = () => {
    const topScroll =
      window.pageYOffset || window.document.documentElement.scrollTop;
    const leftScroll =
      window.pageXOffset || window.document.documentElement.scrollLeft;

    window.onscroll = function () {
      window.scrollTo(topScroll, leftScroll);
    };
  };

  const onMouseLeave = () => {
    window.onscroll = function () {};
  };

  useEffect(() => {
    const fetchMessage = () => {
      if (loadMessage) {
        messageApi
          .getAllMessage({ user_id: user.id, admin_id: 1 })
          .then((res) => {
            if (res.status === config.response.SUCCESS) {
              setMessages(res.data);
            }
          })
          .catch((err) => {});
      }
    };

    fetchMessage();
    setLoadMessage(false);
  }, [loadMessage, user.id]);

  useEffect(() => {
    scrollToBottom();
  }, [messages]);

  useEffect(() => {
    socket.on("receive_message", (data) => {
      if (data.user_id === user.id) {
        setMessages((prev) => [...prev, data]);
      }
    });
  }, [user.id]);

  const handleSend = () => {
    const data = {
      message: chat,
      user_id: user.id,
      admin_id: 1,
      type: "USER_SEND",
    };

    socket.emit("send_message", data);
    setChat("");
    setMessages([...messages, data]);
  };

  const checkEnter = (e) => {
    if (e.keyCode === 13) {
      handleSend();
    }
  };

  return (
    <>
      <div className="btn-chat" onClick={toggleChat}>
        <i className="fas fa-comments"></i>
      </div>

      <div
        className={clsx("popup-chat", { active: openChat })}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
      >
        <div className="box-chat">
          <div className="header-chat">
            <div className="header-chat-avatar">
              <img
                className="img-user-header"
                src={adminImg}
                alt="admin-avatar"
              />
            </div>
            <div className="header-chat-info">
              <div className="name">Admin</div>
              <div className="status">Admin</div>
            </div>
            <div className="header-chat-feature">
              <svg
                role="presentation"
                height="50px"
                width="50px"
                viewBox="-5 -5 30 30"
              >
                <path
                  d="M19.492 4.112a.972.972 0 00-1.01.063l-3.052 2.12a.998.998 0 00-.43.822v5.766a1 1 0 00.43.823l3.051 2.12a.978.978 0 001.011.063.936.936 0 00.508-.829V4.94a.936.936 0 00-.508-.828zM10.996 18A3.008 3.008 0 0014 14.996V5.004A3.008 3.008 0 0010.996 2H3.004A3.008 3.008 0 000 5.004v9.992A3.008 3.008 0 003.004 18h7.992z"
                  fill="#bec2c9"
                ></path>
              </svg>
              <svg
                role="presentation"
                height="26px"
                width="26px"
                viewBox="-5 -5 30 30"
              >
                <path
                  d="M10.952 14.044c.074.044.147.086.22.125a.842.842 0 001.161-.367c.096-.195.167-.185.337-.42.204-.283.552-.689.91-.772.341-.078.686-.105.92-.11.435-.01 1.118.174 1.926.648a15.9 15.9 0 011.713 1.147c.224.175.37.43.393.711.042.494-.034 1.318-.754 2.137-1.135 1.291-2.859 1.772-4.942 1.088a17.47 17.47 0 01-6.855-4.212 17.485 17.485 0 01-4.213-6.855c-.683-2.083-.202-3.808 1.09-4.942.818-.72 1.642-.796 2.136-.754.282.023.536.17.711.392.25.32.663.89 1.146 1.714.475.808.681 1.491.65 1.926-.024.31-.026.647-.112.921-.11.35-.488.705-.77.91-.236.17-.226.24-.42.336a.841.841 0 00-.368 1.161c.04.072.081.146.125.22a14.012 14.012 0 004.996 4.996z"
                  fill="#bec2c9"
                ></path>
                <path
                  d="M10.952 14.044c.074.044.147.086.22.125a.842.842 0 001.161-.367c.096-.195.167-.185.337-.42.204-.283.552-.689.91-.772.341-.078.686-.105.92-.11.435-.01 1.118.174 1.926.648.824.484 1.394.898 1.713 1.147.224.175.37.43.393.711.042.494-.034 1.318-.754 2.137-1.135 1.291-2.859 1.772-4.942 1.088a17.47 17.47 0 01-6.855-4.212 17.485 17.485 0 01-4.213-6.855c-.683-2.083-.202-3.808 1.09-4.942.818-.72 1.642-.796 2.136-.754.282.023.536.17.711.392.25.32.663.89 1.146 1.714.475.808.681 1.491.65 1.926-.024.31-.026.647-.112.921-.11.35-.488.705-.77.91-.236.17-.226.24-.42.336a.841.841 0 00-.368 1.161c.04.072.081.146.125.22a14.012 14.012 0 004.996 4.996z"
                  fill="none"
                  stroke="#bec2c9"
                ></path>
              </svg>

              <svg
                onClick={toggleChat}
                id="btn-close-chat"
                height="26px"
                width="26px"
                viewBox="-4 -4 24 24"
              >
                <line
                  stroke="#bec2c9"
                  strokeLinecap="round"
                  strokeWidth="2"
                  x1="2"
                  x2="14"
                  y1="2"
                  y2="14"
                ></line>
                <line
                  stroke="#bec2c9"
                  strokeLinecap="round"
                  strokeWidth="2"
                  x1="2"
                  x2="14"
                  y1="14"
                  y2="2"
                ></line>
              </svg>
            </div>
          </div>

          <div className="body-chat">
            {messages.map((message, key) => (
              <div
                key={key}
                className={clsx("box-mess", {
                  "to-right": message.type === "USER_SEND",
                })}
              >
                <div className="box-image">
                  <img className="img-user" src={adminImg} alt="admin-avatar" />
                </div>
                <div className="mess-content">
                  <p>{message.message}</p>
                </div>
              </div>
            ))}
            <div ref={messagesEndRef} />
          </div>

          <div className="footer-chat">
            <div className="footer-chat-more">
              <svg
                className="a8c37x1j ms05siws hr662l2t b7h9ocf4"
                height="20px"
                viewBox="0 0 38 38"
                width="20px"
              >
                <g fillRule="evenodd">
                  <g transform="translate(-893.000000, -701.000000)">
                    <g transform="translate(709.000000, 314.000000)">
                      <g>
                        <path
                          d="M210.5,405 C209.121,405 208,403.879 208,402.5 C208,401.121 209.121,400 210.5,400 C211.879,400 213,401.121 213,402.5 C213,403.879 211.879,405 210.5,405 M212.572,411.549 C210.428,413.742 206.938,415 203,415 C199.062,415 195.572,413.742 193.428,411.549 C192.849,410.956 192.859,410.007 193.451,409.428 C194.045,408.85 194.993,408.859 195.572,409.451 C197.133,411.047 199.909,412 203,412 C206.091,412 208.867,411.047 210.428,409.451 C211.007,408.859 211.956,408.85 212.549,409.428 C213.141,410.007 213.151,410.956 212.572,411.549 M195.5,400 C196.879,400 198,401.121 198,402.5 C198,403.879 196.879,405 195.5,405 C194.121,405 193,403.879 193,402.5 C193,401.121 194.121,400 195.5,400 M203,387 C192.523,387 184,395.523 184,406 C184,416.477 192.523,425 203,425 C213.477,425 222,416.477 222,406 C222,395.523 213.477,387 203,387"
                          className="crt8y2ji"
                          fill="#0084ff"
                        ></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <div className="footer-chat-message">
              <input
                type="text"
                className="message-content"
                value={chat}
                onChange={(e) => setChat(e.target.value)}
                onKeyUp={checkEnter}
                placeholder="Nhập tin nhắn..."
              ></input>
            </div>
            <div className="footer-chat-send" onClick={handleSend}>
              <svg
                className="crt8y2ji"
                height="1.5rem"
                width="1.5rem"
                viewBox="0 0 24 24"
              >
                <path
                  d="M16.6915026,12.4744748 L3.50612381,13.2599618 C3.19218622,13.2599618 3.03521743,13.4170592 3.03521743,13.5741566 L1.15159189,20.0151496 C0.8376543,20.8006365 0.99,21.89 1.77946707,22.52 C2.41,22.99 3.50612381,23.1 4.13399899,22.8429026 L21.714504,14.0454487 C22.6563168,13.5741566 23.1272231,12.6315722 22.9702544,11.6889879 C22.8132856,11.0605983 22.3423792,10.4322088 21.714504,10.118014 L4.13399899,1.16346272 C3.34915502,0.9 2.40734225,1.00636533 1.77946707,1.4776575 C0.994623095,2.10604706 0.8376543,3.0486314 1.15159189,3.99121575 L3.03521743,10.4322088 C3.03521743,10.5893061 3.34915502,10.7464035 3.50612381,10.7464035 L16.6915026,11.5318905 C16.6915026,11.5318905 17.1624089,11.5318905 17.1624089,12.0031827 C17.1624089,12.4744748 16.6915026,12.4744748 16.6915026,12.4744748 Z"
                  fillRule="evenodd"
                  stroke="none"
                ></path>
              </svg>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Chat;
