import React from "react";
import { WaveTopBottomLoading } from "react-loadingg";

const loading = {
  position: "relative",
  margin: "0 auto",
  width: "30px",
};

function WaveLoading() {
  return (
    <>
      <div style={loading}>
        <WaveTopBottomLoading size="small" color="#5985ff" speed={0.5} />
      </div>
    </>
  );
}

export default WaveLoading;
