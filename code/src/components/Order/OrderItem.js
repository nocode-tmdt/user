import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Button, Popconfirm } from "antd";
import useToggle from "~/hooks/useToggle";
import { useTranslation } from "react-i18next";
import config from "~/constants";
import { formatPrice } from "~/helpers/helpers";
import orderApi from "~/apis/orderApi";
import toast from "~/helpers/toast";

const orderTab = config.orderTab;

const states = orderTab.reduce((states, state) => {
  const { key } = state;
  return { ...states, [key]: key };
}, {});

const statusText = orderTab.reduce((states, state) => {
  const { key, name } = state;
  return { ...states, [key]: name };
}, {});

function OrderItem(props) {
  const { t } = useTranslation();
  const history = useHistory();
  const [openPop, togglePop] = useToggle(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const { order, index } = props;
  const { shipping, code, total_money } = order;

  const handleClick = (id) => {
    history.push(config.routes.ORDER_DETAIL + id);
  };

  const onCancelOrder = () => {
    setConfirmLoading(true);

    orderApi
      .updateStatus({ code, status: "canceled" })
      .then((res) => {
        if (res.status === config.response.SUCCESS) {
          togglePop();
          setConfirmLoading(false);
          toast.success("Thành công", "Hủy đơn hàng thành công");
          history.go(0);
        }
      })
      .catch((err) => {
        togglePop();
        setConfirmLoading(false);
      });
  };

  const continePayment = () => {
    setConfirmLoading(true);
    const newOrder = { code, total_money };
    const { method } = shipping;

    if (method === 1) {
      orderApi
        .orderVNPay(newOrder)
        .then((resVNPay) => {
          window.location.href = resVNPay.data.url;
        })
        .catch((err) => {
          setConfirmLoading(false);
        });
    }

    if (method === 2) {
      orderApi
        .orderMomo(newOrder)
        .then((resMomo) => {
          window.location.href = resMomo.data.url;
        })
        .catch((err) => {
          setConfirmLoading(false);
        });
    }
  };

  return (
    <tr>
      <td>{index}</td>
      <td>{code}</td>
      <td>{order.user.name}</td>
      <td>{statusText[order.status]}</td>
      <td>{formatPrice(order.total_money)}</td>
      <td>{order.time}</td>
      <td>
        {order.status === states.processing && (
          <Popconfirm
            title="Bạn có muổn hủy đơn hàng ?"
            visible={openPop}
            onConfirm={onCancelOrder}
            okButtonProps={{ loading: confirmLoading }}
            onCancel={togglePop}
            okText={t("common.yes")}
            cancelText={t("common.no")}
          >
            <Button
              type="dashed"
              danger
              onClick={togglePop}
              style={{
                backgroundColor: "transparent",
                marginRight: "6px",
              }}
            >
              Hủy
            </Button>
          </Popconfirm>
        )}

        {order.status === states.awaiting_payment && (
          <Button
            type="primary"
            loading={confirmLoading}
            onClick={continePayment}
            style={{
              backgroundColor: "transparent",
              marginRight: "6px",
              color: "#4BB543",
              borderColor: "#4BB543",
            }}
          >
            Thanh toán
          </Button>
        )}

        <Button
          type="primary"
          onClick={() => handleClick(order.id)}
          style={{ backgroundColor: "transparent", color: "#2577fd" }}
        >
          Chi tiết
        </Button>
      </td>
    </tr>
  );
}

export default OrderItem;
