import React, { useState } from "react";
import { Modal, Button } from "antd";
import { useFormik } from "formik";
import { useDispatch } from "react-redux";
import { authToken } from "~/actions/action";
import userApi from "~/apis/userApi";
import config from "~/constants";
import toast from "~/helpers/toast";
import { profileValid } from "~/helpers/validate";

function ModalProfile(props) {
  const { user, openInfo, toggleInfo } = props;
  const [confirmLoading, setConfirmLoading] = useState(false);
  const dispatch = useDispatch();

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: user.name || "",
      email: user.email || "",
      phone: user.phone || "",
      status: user.status || "",
      address: user.address || "",
    },
    validationSchema: profileValid,
    onSubmit: (value) => {
      setConfirmLoading(true);
      userApi
        .updateProfile(value)
        .then((res) => {
          if (res.status === config.response.SUCCESS) {
            toggleInfo();
            dispatch(authToken());
            toast.success("Thành công", "Cập nhập thành công");
          } else {
            toggleInfo();
            toast.success("Thất bại", "Cập nhập thât bại");
          }
          setConfirmLoading(false);
        })
        .catch((err) => {
          toggleInfo();
          setConfirmLoading(false);
          toast.success("Thất bại", "Cập nhập thât bại");
        });
    },
  });

  return (
    <Modal visible={openInfo} onCancel={toggleInfo} footer={null}>
      <div className="form-section">
        <h3 className="user-info">Cập nhập thông tin</h3>
        <form onSubmit={formik.handleSubmit} className="form-info">
          <div className="form-group">
            <div className="input-field">
              <i className="fas fa-user" />
              <input
                type="text"
                name="name"
                className="form-control"
                value={formik.values.name}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                placeholder="Tên người dùng..."
              />
            </div>
            {formik.errors.name && formik.touched.name && (
              <p className="error">{formik.errors.name}</p>
            )}
          </div>
          <div className="form-group">
            <div className="input-field">
              <i className="fas fa-envelope" />
              <input
                type="text"
                name="email"
                disabled
                className="form-control"
                value={formik.values.email}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                placeholder="Email người dùng..."
              />
            </div>
            {formik.errors.email && formik.touched.email && (
              <p className="error">{formik.errors.email}</p>
            )}
          </div>
          <div className="form-group">
            <div className="input-field">
              <i className="fas fa-mobile" />
              <input
                type="text"
                name="phone"
                className="form-control"
                value={formik.values.phone}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                placeholder="Số điện thoại người dùng..."
              />
            </div>
            {formik.errors.phone && formik.touched.phone && (
              <p className="error">{formik.errors.phone}</p>
            )}
          </div>

          <div className="form-group">
            <div className="input-field">
              <i className="fas fa-info-circle"></i>
              <input
                type="text"
                name="status"
                className="form-control"
                value={formik.values.status}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                placeholder="Trạng thái người dùng..."
              />
            </div>
            {formik.errors.status && formik.touched.status && (
              <p className="error">{formik.errors.status}</p>
            )}
          </div>
          <div className="form-group">
            <div className="input-field">
              <i className="fas fa-map-marked-alt"></i>
              <input
                type="text"
                name="address"
                className="form-control"
                value={formik.values.address}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                placeholder="Địa chỉ người dùng..."
              />
            </div>
            {formik.errors.address && formik.touched.address && (
              <p className="error">{formik.errors.address}</p>
            )}
          </div>
          <div className="order-button">
            <Button
              className="btn btn-order"
              loading={confirmLoading}
              htmlType="submit"
            >
              Cập nhập
            </Button>
          </div>
        </form>
      </div>
    </Modal>
  );
}

export default ModalProfile;
