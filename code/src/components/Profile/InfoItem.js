import React from "react";

function InfoItem(props) {
  const { icon, title, content } = props;

  return (
    <div className="info-item">
      <div className="info-icon">{icon}</div>
      <div className="info-left">
        <p className="info-title">{title}</p>
        <h4 className="info-content">{content}</h4>
      </div>
      <div className="info-icon">
        <i className="fas fa-edit"></i>
      </div>
    </div>
  );
}

export default InfoItem;
