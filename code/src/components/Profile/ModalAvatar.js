import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useTranslation } from "react-i18next";
import { Modal, Upload, Progress } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import userApi from "~/apis/userApi";
import toast from "~/helpers/toast";
import config from "~/constants";
import { authToken } from "~/actions/action";

function ModalAvatar(props) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { openAvatar, toggleAvatar } = props;
  const [defaultFileList, setDefaultFileList] = useState([]);
  const [progress, setProgress] = useState(0);

  const uploadImage = async (options) => {
    const { onSuccess, onError, file, onProgress } = options;

    const fmData = new FormData();
    const configReq = {
      headers: { "content-type": "multipart/form-data" },
      onUploadProgress: (event) => {
        const percent = Math.floor((event.loaded / event.total) * 100);
        setProgress(percent);
        if (percent === 100) {
          setTimeout(() => setProgress(0), 1000);
        }
        onProgress({ percent: (event.loaded / event.total) * 100 });
      },
    };
    fmData.append("image", file);

    userApi
      .updateAvatar(fmData, configReq)
      .then((res) => {
        if (res.status === config.response.SUCCESS) {
          onSuccess(t("common.success"));
          dispatch(authToken());
          toast.success(
            t("common.success"),
            t("profile.avatar_update_success")
          );
          toggleAvatar();
        } else {
          toggleAvatar();
          toast.success(t("common.fail"), t("profile.avatar_update_fail"));
        }
      })
      .catch((err) => {
        onError("Thất bại");
        toggleAvatar();
        toast.success(t("common.fail"), t("profile.avatar_update_fail"));
      });
  };

  const handleOnChange = ({ fileList }) => {
    setDefaultFileList(fileList);
  };
  return (
    <Modal visible={openAvatar} onCancel={toggleAvatar} footer={null}>
      <div className="form-section">
        <h3 className="user-info">{t("profile.profile_avatar_update")}</h3>
        <Upload
          name="image"
          multiple={false}
          accept="image/*"
          customRequest={uploadImage}
          listType="picture-card"
          className="image-upload-grid"
          onChange={handleOnChange}
          defaultFileList={defaultFileList}
        >
          {defaultFileList.length >= 1 ? null : <UploadOutlined />}
        </Upload>
        {progress > 0 ? <Progress percent={progress} /> : null}
      </div>
    </Modal>
  );
}

export default ModalAvatar;
