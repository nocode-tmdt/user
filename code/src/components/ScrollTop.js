import React, { useEffect, useRef } from "react";

function ScrollTop() {
  const scrollTop = useRef();

  useEffect(() => {
    window.onscroll = () => {
      if (window.pageYOffset > 100)
        scrollTop.current.style.animation = "scrollTopIn ease-in 0.4s";
      else scrollTop.current.style.animation = "scrollTopOut ease-in 0.4s";

      scrollTop.current.style.animationFillMode = "forwards";
    };
  }, []);

  const handleClick = () => {
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
  };

  return (
    <div className="scroll-top" onClick={handleClick} ref={scrollTop}>
      <i className="fas fa-angle-up" />
    </div>
  );
}

export default ScrollTop;
