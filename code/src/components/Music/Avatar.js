import React from "react";

function Avatar(props) {
  const { tracks, idx } = props;

  return (
    <>
      <img
        src={tracks[idx].cover}
        className="music-avatar1"
        alt={tracks[idx].name}
      />
      <img
        src={tracks[idx].cover}
        className="music-avatar"
        alt={tracks[idx].name}
      />
      <h4 className="music-name">{tracks[idx].artist}</h4>
      <h1 className="music-title">{tracks[idx].name}</h1>
    </>
  );
}

export default Avatar;
