import React from "react";
import {
  PauseFill,
  PlayFill,
  SkipBackwardFill,
  SkipForwardFill,
} from "react-bootstrap-icons";

function Control(props) {
  const { idx, setIdx, playState, setPlayState, tracks } = props;

  return (
    <div className="music-controls">
      <button
        className="music-controlButton"
        onClick={(x) => setIdx(idx - 1 < 0 ? tracks.length - 1 : idx - 1)}
      >
        <SkipBackwardFill />
      </button>
      {playState === true ? (
        <button
          className="music-centerButton"
          onClick={(x) => setPlayState(false)}
        >
          <PauseFill />
        </button>
      ) : (
        <button
          className="music-centerButton"
          onClick={(x) => setPlayState(true)}
        >
          <PlayFill />
        </button>
      )}
      <button
        className="music-controlButton"
        onClick={(x) => setIdx(idx + 1 > tracks.length - 1 ? 0 : idx + 1)}
      >
        <SkipForwardFill />
      </button>
    </div>
  );
}

export default Control;
