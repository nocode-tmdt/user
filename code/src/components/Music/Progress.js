import React, { useState, useContext, useRef } from "react";

function Progress(props) {
  const { player, userOptions } = props;
  let [currLength, setCurrLength] = useState(0);
  let widthProgress = useRef("0px");
  let [length, setLength] = useState(0);
  let options = useContext(userOptions);

  function updateProgress(e) {
    let offset = e.target.getBoundingClientRect().left;
    let newOffSet = e.clientX;
    let newWidth = newOffSet - offset;
    widthProgress.current = newWidth + "px";
    let secPerPx = length / 280;
    player.currentTime = secPerPx * newWidth;
  }

  setInterval(() => {
    setLength(Math.ceil(player.duration));
    setCurrLength(Math.ceil(player.currentTime));
    let secPerPx = Math.ceil(player.duration) / 280;
    let newWidth = player.currentTime / secPerPx;
    widthProgress.current = newWidth + "px";
    if (player.currentTime === player.duration) {
      if (options.shuffle === true) {
        props.setIdx(parseInt(Math.random() * 1000) % 9);
      } else if (options.repeat === true) {
        player.play();
      } else {
        props.setIdx((props.idx + 1) % 9);
      }
    }
  }, 1000);

  function formatTime(s) {
    return Number.isNaN(s)
      ? "0:00"
      : (s - (s %= 60)) / 60 + (9 < s ? ":" : ":0") + s;
  }

  return (
    <div className="music-progress">
      <div className="music-currentTime">
        <p>{formatTime(currLength)}</p>
      </div>
      <div className="music-progressCenter" onClick={(e) => updateProgress(e)}>
        <div
          className="music-progressBar"
          style={{
            width: widthProgress.current,
          }}
        ></div>
      </div>
      <div className="music-songLength">
        <p>{formatTime(length)}</p>
      </div>
    </div>
  );
}

export default Progress;
