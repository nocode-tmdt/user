import React, { useEffect, useState, useRef } from "react";
import Avatar from "./Avatar";
import Progress from "./Progress";
import Control from "./Control";
import Options from "./Options";

function Container(props) {
  const { player, tracks, userOptions } = props;
  let [idx, setIdx] = useState(0);
  let [playState, setPlayState] = useState(false);
  let oldIdx = useRef(idx);

  useEffect(() => {
    if (playState === true) player.play();
    else player.pause();
    if (idx !== oldIdx.current) {
      player.pause();
      player.src = tracks[idx].source;
      player.load();
      player.play();
      setPlayState(true);
      oldIdx.current = idx;
    }
  }, [playState, player, idx, tracks]);

  return (
    <div className="music-playerContaier">
      <Avatar tracks={tracks} idx={idx} />
      <Progress
        player={player}
        setIdx={setIdx}
        idx={idx}
        userOptions={userOptions}
      />
      <Control
        setIdx={setIdx}
        idx={idx}
        tracks={tracks}
        playState={playState}
        setPlayState={setPlayState}
      />
      <Options
        userOptions={userOptions}
        tracks={tracks}
        setIdx={setIdx}
        idx={idx}
      />
    </div>
  );
}

export default Container;
