import React, { useState } from "react";
import { Rate, Button } from "antd";
import toast from "~/helpers/toast";
import config from "~/constants";
import commentApi from "~/apis/commentApi";

function CommentInput(props) {
  const {
    cancelModal,
    handleSetUpdate,
    productId,
    comment,
    title,
    buttonText,
  } = props;
  const [commentStar, setCommentStar] = useState(comment.star || 0);
  const [commentText, setCommentText] = useState(comment.comment || "");
  const [confirmLoading, setConfirmLoading] = useState(false);

  const handleComment = () => {
    if (commentStar < 1 || commentStar > 5) {
      return toast.warning("Thất bại", "Chất lượng phải từ 1 đến 5 sao");
    }

    if (commentText.length < 2 || commentText.length > 100) {
      return toast.warning("Thất bại", "Nội dung bình luận quá ngắn");
    }

    setConfirmLoading(true);
    const values = {
      product_id: productId,
      star: commentStar,
      comment: commentText,
    };

    commentApi
      .updateUserComment(values)
      .then((res) => {
        if (cancelModal) cancelModal();

        if (res.status === config.response.SUCCESS) {
          handleSetUpdate();
          setConfirmLoading(false);
          return toast.success("Thành công", "Cập nhập bình luận thành công");
        }

        setConfirmLoading(false);
        return toast.error("Thất bại", "Cập nhập bình luận thất bại");
      })
      .catch((err) => {
        if (cancelModal) cancelModal();
        setConfirmLoading(false);
        return toast.error("Thất bại", "Cập nhập bình luận thất bại");
      });
  };

  return (
    <div className="comment-input-box">
      <h3 className="title-edit">{title || "Thêm bình luận"}</h3>
      <h4 className="quality">Chất lượng: </h4>
      <div className="rating">
        <Rate
          character={<i className="fas fa-star" />}
          value={commentStar}
          onChange={(value) => setCommentStar(value)}
        />
      </div>
      <h4 className="title-comment">Bình luận của bạn: </h4>
      <textarea
        className="comment-input"
        cols={54}
        rows={10}
        value={commentText}
        onChange={(e) => setCommentText(e.target.value)}
      />
      <div className="btn-comment-wrapper">
        <Button
          className="btn btn-comment"
          loading={confirmLoading}
          onClick={handleComment}
        >
          {buttonText || "Bình luận"}
        </Button>
      </div>
    </div>
  );
}

export default CommentInput;
