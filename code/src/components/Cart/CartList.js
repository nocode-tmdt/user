import React from "react";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import CartItem from "./CartItem";

function CartList(props) {
  const carts = useSelector((state) => state.cartReducer.carts);
  const { t } = useTranslation();

  return (
    <table className="table">
      <thead>
        <tr>
          <th>#</th>
          <th>{t("common.image")}</th>
          <th>{t("common.name")}</th>
          <th>{t("common.quantity")}</th>
          <th>{t("common.price")}</th>
          <th>{t("common.total_money")}</th>
          <th>{t("common.action")}</th>
        </tr>
      </thead>
      <tbody>
        {carts.map((cart) => (
          <CartItem key={cart.id} cart={cart} />
        ))}
      </tbody>
    </table>
  );
}

export default CartList;
