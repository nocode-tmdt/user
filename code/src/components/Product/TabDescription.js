import React from "react";
import { Tabs } from "antd";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { isEmpty } from "lodash";
import parse from "html-react-parser";
import FacebookComment from "~/components/Facebook/FacebookComment";
import CommentInput from "~/components/Comment/CommentInput";
import CommentList from "~/components/Comment/CommentList";

const { TabPane } = Tabs;

function TabDescription(props) {
  const { product, post, comments, yourComment, handleSetUpdate } = props;
  const { t } = useTranslation();
  const isAuth = useSelector((state) => state.userReducer.isAuth);

  return (
    <>
      <h2 className="title-section">Chi tiết</h2>
      <ul className="tab-description">
        <Tabs defaultActiveKey="1">
          <TabPane
            tab={t("common.description")}
            key="1"
            className="content-desciption"
          >
            {product.description}
          </TabPane>
          <TabPane
            tab={t("common.post")}
            key="2"
            className="content-desciption"
          >
            {/* <div dangerouslySetInnerHTML={{ __html: test.content }} /> */}
            {!isEmpty(post)
              ? parse(post.title + post.content)
              : t("product.no_posts")}
          </TabPane>
          <TabPane
            tab={t("product.facebook_comments")}
            key="3"
            className="content-desciption"
          >
            <FacebookComment />
          </TabPane>
          <TabPane
            tab={t("product.all_comments")}
            key="4"
            className="content-desciption"
          >
            <div className="tab-all-comment">
              {comments.length ? (
                <CommentList
                  comments={comments}
                  handleSetUpdate={handleSetUpdate}
                />
              ) : (
                t("product.no_comments")
              )}
            </div>
          </TabPane>
          {isAuth && (
            <TabPane
              tab={t("product.your_comment")}
              key="5"
              className="content-desciption"
            >
              {!isEmpty(yourComment) ? (
                <CommentList
                  comments={yourComment}
                  handleSetUpdate={handleSetUpdate}
                />
              ) : (
                <CommentInput
                  productId={product.id}
                  comment={{}}
                  handleSetUpdate={handleSetUpdate}
                />
              )}
            </TabPane>
          )}
        </Tabs>
      </ul>
    </>
  );
}

export default TabDescription;
