import React from "react";
import ProductItem from "./ProductItem";

function ProductList(props) {
  const { products } = props;

  return (
    <div className="product-list row">
      {products.map((product, key) => (
        <ProductItem key={key} product={product} />
      ))}
    </div>
  );
}

export default ProductList;
