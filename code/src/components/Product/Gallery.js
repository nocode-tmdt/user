import React, { useState } from "react";
import Slider from "react-slick";
import config from "~/constants";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

function Gallery(props) {
  const { gallerys, product } = props;
  const [nav1, setNav1] = useState();
  const [nav2, setNav2] = useState();

  return (
    <div className="pro-gallery">
      <div className="pro-gallery-single">
        {gallerys.length ? (
          <Slider
            infinite={false}
            asNavFor={nav2}
            swipeToSlide={true}
            ref={(slider1) => setNav1(slider1)}
          >
            {gallerys.map((gallery) => (
              <div key={gallery.id}>
                <img
                  style={{ height: "350px" }}
                  src={config.api.GALLERY_IMAGE + gallery.image}
                  alt={product.name}
                />
              </div>
            ))}
          </Slider>
        ) : (
          <img
            src={config.api.PRODUCT_IMAGE + product.image}
            alt={product.name}
          />
        )}
      </div>
      {!!gallerys.length && (
        <div className="pro-gallery-tab">
          <Slider
            infinite={false}
            asNavFor={nav1}
            ref={(slider2) => setNav2(slider2)}
            slidesToShow={5}
            swipeToSlide={true}
            focusOnSelect={true}
            slidesToScroll={5}
            className="small-slick"
          >
            {gallerys.map((gallery) => (
              <div className="gallery-image-wrapper item" key={gallery.id}>
                <img
                  src={config.api.GALLERY_IMAGE + gallery.image}
                  alt={product.name}
                />
              </div>
            ))}
          </Slider>
        </div>
      )}
    </div>
  );
}

export default Gallery;
