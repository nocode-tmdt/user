import React from "react";
import { Rate } from "antd";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import config from "~/constants";
import { formatPrice } from "~/helpers/helpers";

function ProductItem(props) {
  const { product } = props;
  const { t } = useTranslation();

  return (
    <div className="col-6 col-md-4 col-lg-3 col-xl-3">
      <Link to={config.routes.PRODUCT + product.slug} className="product">
        <p className="pro-view">
          {product.visit} <i className="fas fa-eye" />
        </p>
        {/* <p className="pro-new">{product.feather ? "Nổi bật" : "Mới"}</p> */}
        <div className="pro-img">
          <img
            src={config.api.PRODUCT_IMAGE + product.image}
            alt={product.name}
          />
        </div>

        <div className="pro-content">
          <div className="pro-info">
            <p className="pro-detail">{t("product.view_detail")}</p>
            <p className="pro-name"> {product.name}</p>
            <div className="pro-price">
              <div className="pro-prev-price">{t("product.discount")}10%</div>
              <p className="pro-current-price">{formatPrice(product.price)}</p>
            </div>
            <div className="pro-more">
              <div className="pro-star">
                <Rate
                  disabled
                  character={<i className="fas fa-star" />}
                  value={product.star}
                />
              </div>
              <p className="pro-sell">
                {t("product.sold")}
                {product.visit}
              </p>
            </div>
          </div>
        </div>
      </Link>
    </div>
  );
}

export default ProductItem;
