import React, { useEffect } from "react";
import { InlineShareButtons } from "sharethis-reactjs";
import { initFacebookSDK, cacheFacebook } from "~/services/facebookService";

function FacebookLikeShare() {
  const currentURL = window.location.href;

  useEffect(() => {
    initFacebookSDK(currentURL);
    cacheFacebook(currentURL);
  }, [currentURL]);

  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <InlineShareButtons
        config={{
          alignment: "left",
          color: "social",
          enabled: true,
          font_size: 14,
          language: "en",
          networks: [
            "facebook",
            "messenger",
            "email",
            "linkedin",
            "twitter",
            "whatsapp",
            "reddit",
          ],
          padding: 12,
          radius: 4,
          show_total: true,
          size: 25,

          url: currentURL,
        }}
      />
      <div style={{ display: "flex", marginLeft: "-10px", marginTop: "6px" }}>
        <div
          className="fb-like"
          data-href={currentURL}
          data-width=""
          data-layout="button_count"
          data-action="like"
          data-size="small"
          data-share="false"
        ></div>
        <div
          className="fb-share-button"
          data-href={currentURL}
          data-layout="button_count"
          data-size="small"
        >
          <a
            target="_blank"
            rel="noreferrer"
            href={`https://www.facebook.com/sharer/sharer.php?u=${currentURL}&amp;src=sdkpreparse`}
            className="fb-xfbml-parse-ignore"
          >
            Chia sẻ
          </a>
        </div>

        <div
          style={{ marginLeft: "8px" }}
          className="zalo-follow-only-button ml-2"
          data-oaid={2905292136695329731}
        />
        <div
          style={{ marginLeft: "-14px" }}
          className="zalo-share-button"
          data-href={currentURL}
          data-oaid={2905292136695329731}
          data-layout={1}
          data-color="blue"
          data-customize="false"
        />
      </div>
    </div>
  );
}

export default FacebookLikeShare;
