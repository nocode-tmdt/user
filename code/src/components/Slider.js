import React from "react";
import { Carousel } from "antd";
import { Link } from "react-router-dom";
import config from "~/constants";

function Slider(props) {
  const { sliders } = props;

  return (
    <div className="slider">
      <div className="slider-img">
        <Carousel autoplaySpeed={2000} autoplay draggable dots={null}>
          {sliders.map((slider, index) => (
            <div style={{ height: "100%" }} key={index}>
              <Link to={config.routes.PRODUCT + slider.product.slug} key={slider.id}>
                <img
                  src={config.api.SLIDER_IMAGE + slider.image}
                  alt={slider.product.name}
                />
              </Link>
            </div>
          ))}
        </Carousel>
      </div>
    </div>
  );
}

export default Slider;
