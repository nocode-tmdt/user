import React, { useState, useEffect } from "react";
import { Select, Radio, Typography } from "antd";
import className from "classnames/bind";
import moment from "moment";
import "moment/locale/vi";
import addressApi from "~/apis/addressApi";
import ghnIcon from "~/assets/img/delivery/ghn.png";
import styles from "~/themes/Address/Address.module.scss";

const { Text } = Typography;
const cx = className.bind(styles);
const { Option } = Select;

moment.locale("vi");

function Address(props) {
  const { setFeeship, setAddress } = props;
  const [provinces, setProvinces] = useState([]);
  const [currentProvince, setCurrentProvince] = useState("");
  const [districts, setDistricts] = useState([]);
  const [currentDistrict, setCurrentDistrict] = useState("");
  const [wards, setWards] = useState([]);
  const [currentWard, setCurrentWard] = useState("");
  const [services, setServices] = useState([]);
  const [currentService, setCurrentService] = useState("");
  const [messasge, setMessage] = useState("");
  const [estimateTime, setEstimateTime] = useState("");

  useEffect(() => {
    addressApi
      .getProvince()
      .then((res) => {
        console.log("Province data: ", res.data);
        setProvinces(res.data);
        setCurrentProvince(res.data[0].ProvinceID);
      })
      .catch((err) => console.log(err));
  }, []);

  useEffect(() => {
    if (currentProvince === "") return;

    const province = provinces.find(
      (province) => province.ProvinceID === currentProvince
    );

    console.log("provinceName", province.ProvinceName);
    setAddress(province.ProvinceName);

    addressApi
      .getDistrict(currentProvince)
      .then((res) => {
        console.log("District data: ", res.data);

        const data = res.data;
        if (data[0].DistrictID === 3450) {
          data.shift();
        }
        if (data[0].DistrictID === 3442) {
          data.shift();
        }

        console.log("Data handle: ", data);
        setDistricts(data);
        setCurrentDistrict(data[0].DistrictID);
      })
      .catch((err) => console.log(err));
  }, [currentProvince]);

  useEffect(() => {
    if (currentDistrict === "") return;

    const province = provinces.find(
      (province) => province.ProvinceID === currentProvince
    );
    const district = districts.find(
      (district) => district.DistrictID === currentDistrict
    );

    setAddress(`${province.ProvinceName}, ${district.DistrictName}`);

    addressApi
      .getWard(currentDistrict)
      .then((res) => {
        console.log("Ward data: ", res.data);
        setWards(res.data);
        setCurrentWard(res.data[0].WardCode);
      })
      .catch((err) => console.log(err));

    addressApi
      .getAvailableServices(currentDistrict)
      .then((res) => {
        console.log("Services data: ", res);
        setMessage(res.code_message_value);
        setServices(res.data);
        setCurrentService(res.data[0].service_id);
      })
      .catch((err) => console.log(err));
  }, [currentDistrict]);

  useEffect(() => {
    if (currentService === "" || currentWard === "") return;

    const province = provinces.find(
      (province) => province.ProvinceID === currentProvince
    );
    const district = districts.find(
      (district) => district.DistrictID === currentDistrict
    );
    const ward = wards.find((ward) => ward.WardCode === currentWard);

    setAddress(
      `${province.ProvinceName}, ${district.DistrictName}, ${ward.WardName}`
    );
  }, [currentWard]);

  useEffect(() => {
    if (currentService === "" || currentWard === "") return;

    const data = {
      service_id: currentService,
      insurance_value: 500000,
      to_district_id: currentDistrict,
      to_ward_code: parseInt(currentWard),
    };

    console.log("Data leadtime: ", data);

    addressApi
      .getFeeship(data)
      .then((res) => {
        console.log("Feeship data: ", res);
        setFeeship(res.data.total);
      })
      .catch((err) => console.log(err));

    addressApi
      .getEstimateTime(data)
      .then((res) => {
        console.log("EstimateTime: ", res.data);
        setEstimateTime(res.data.leadtime);
      })
      .catch((err) => console.log(err));
  }, [currentService, currentWard]);

  const handleChangeProvince = (provinceID, option) => {
    setCurrentProvince(provinceID);
    console.log("Change Province ID: ", provinceID, option);
  };

  const handleChangeDistrict = (DistrictID, option) => {
    setCurrentDistrict(DistrictID);
    console.log("Change DistrictID: ", DistrictID, option);
  };

  const handleChangeWard = (WardID, option) => {
    setCurrentWard(WardID);
    console.log("Change WardID: ", WardID, option);
  };

  return (
    <div className="address-section">
      <h3 className="title-address">
        Giao hàng
        <div className={cx("ghn-icon")}>
          <img src={ghnIcon} alt="giao hang nhanh icon" />
        </div>
      </h3>
      <div className="row">
        <div className="col-12 col-md-3 col-lg-3 col-xl-3">
          <p className="choose-address">Tỉnh /Thành phố</p>

          <Select
            showSearch
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            filterSort={(optionA, optionB) =>
              optionA.children
                .toLowerCase()
                .localeCompare(optionB.children.toLowerCase())
            }
            value={currentProvince}
            onChange={handleChangeProvince}
          >
            {provinces &&
              provinces.length > 0 &&
              provinces.map((province) => (
                <Option key={province.ProvinceID} value={province.ProvinceID}>
                  {province.ProvinceName}
                </Option>
              ))}
          </Select>
        </div>
        <div className="col-12 col-md-3 col-lg-3 col-xl-3">
          <p className="choose-address">Quận /Huyện</p>

          <Select
            showSearch
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            filterSort={(optionA, optionB) =>
              optionA.children
                .toLowerCase()
                .localeCompare(optionB.children.toLowerCase())
            }
            value={currentDistrict}
            onChange={handleChangeDistrict}
          >
            {districts &&
              districts.length > 0 &&
              districts.map((district) => (
                <Option key={district.DistrictID} value={district.DistrictID}>
                  {district.DistrictName}
                </Option>
              ))}
          </Select>
        </div>
        <div className="col-12 col-md-3 col-lg-3 col-xl-3">
          <p className="choose-address">Xã /Phường</p>

          <Select
            showSearch
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            filterSort={(optionA, optionB) =>
              optionA.children
                .toLowerCase()
                .localeCompare(optionB.children.toLowerCase())
            }
            value={currentWard}
            onChange={handleChangeWard}
          >
            {wards &&
              wards.length > 0 &&
              wards.map((ward) => (
                <Option key={ward.WardCode} value={ward.WardCode}>
                  {ward.WardName}
                </Option>
              ))}
          </Select>
        </div>
      </div>

      <h3 className={cx("title-address")}>Hình thức vận chuyển</h3>
      <div className="row">
        <div className="col-12">
          <ul className={cx("method-list")}>
            <Radio.Group
              value={currentService}
              onChange={(e) => setCurrentService(e.target.value)}
            >
              <div className="method-box">
                {services &&
                  services.length > 0 &&
                  services.map((service) => (
                    <li className="method-item" key={service.service_id}>
                      <Radio value={service.service_id}>
                        {service.short_name}
                      </Radio>
                    </li>
                  ))}
              </div>
            </Radio.Group>
          </ul>
        </div>
        <Text type="success">Thời gian giao hàng dự kiến: </Text>
        {estimateTime && (
          <Text type="success">
            {" "}
            {moment.unix(estimateTime).format("LLLL")}
          </Text>
        )}
        {messasge && <Text type="secondary">{messasge}</Text>}
      </div>
    </div>
  );
}

export default Address;
