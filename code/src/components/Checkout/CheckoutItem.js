import React from "react";
import { Link } from "react-router-dom";
import config from "~/constants";
import { formatPrice } from "~/helpers/helpers";

function CheckoutItem(props) {
  const { cart, index } = props;
  const { product } = cart;

  return (
    <tr>
      <td>{index}</td>
      <td>
        <Link to={config.routes.PRODUCT + product.slug} className="pro-img">
          <img
            src={config.api.PRODUCT_IMAGE + product.image}
            alt={product.name}
          />
        </Link>
      </td>
      <td>
        <Link to={config.routes.PRODUCT + product.slug} className="pro-name">
          {product.name}
        </Link>
      </td>
      <td>{cart.quantity}</td>
      <td>{formatPrice(product.price)}</td>
      <td>{formatPrice(product.price * cart.quantity)}</td>
    </tr>
  );
}

export default CheckoutItem;
