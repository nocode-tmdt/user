import config from "~/constants";

const initialState = {
  carts: null,
  totalPrice: 0,
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case config.types.INIT_CART: {
      const { carts, total_price: totalPrice } = action.payload;
      return { carts, totalPrice };
    }

    case config.types.DELETE_ALL_CART: {
      return { carts: [], totalPrice: 0 };
    }

    default:
      return state;
  }
};

export default cartReducer;
