import config from "~/constants";

const rssFeed = config.rssFeed;

const initialState = rssFeed.reduce((states, state) => {
  const { key } = state;
  return { ...states, [key]: null };
}, {});

const newsReducer = (state = initialState, action) => {
  switch (action.type) {
    case config.types.UPDATE_TAB: {
      const { key, news } = action.payload;
      return { ...state, [key]: news };
    }

    default:
      return state;
  }
};

export default newsReducer;
