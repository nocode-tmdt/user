const axios = require("axios");

require("babel-register")({
  presets: ["es2015", "react"],
});

const router = require("./router").default;
const Sitemap = require("react-router-sitemap").default;

async function generateSitemap() {
  try {
    const products = await axios.get(
      "https://api.hstore.pw/api/product/get-all"
    );
    const slugs = products.data.data.map((product) => ({ slug: product.slug }));

    const paramsConfig = {
      "/product/:slug": slugs,
    };

    return new Sitemap(router)
      .applyParams(paramsConfig)
      .build("https://hstore.pw")
      .save("./public/sitemap.xml");
  } catch (e) {
    console.log(e);
  }
}

generateSitemap();
