import React from "react";
import { Route, Switch } from "react-router-dom";
import config from "~/constants";

import NotFound from "~/pages/NotFound";

export default (
  <Switch>
    <Route path={config.routes.SIGN_UP} component={NotFound} />
    <Route exact path={config.routes.HOME} component={NotFound} />
    <Route path={config.routes.PRODUCT + ":slug"} component={NotFound} />
    <Route exact path={config.routes.SIGN_IN} component={NotFound} />
    <Route path={config.routes.SIGN_IN_SOCIAL_CB} component={NotFound} />
    <Route path={config.routes.SEARCH} component={NotFound} />
    <Route path={config.routes.YOUTUBE_SEARCH} component={NotFound} />
    <Route path={config.routes.CONTACT} component={NotFound} />
    <Route path={config.routes.NEWS} component={NotFound} />
    <Route path={config.routes.PROFILE} component={NotFound} />
    <Route path={config.routes.ACTIVITY} component={NotFound} />
    <Route path={config.routes.CART} component={NotFound} />
    <Route path={config.routes.CHECKOUT} component={NotFound} />
    <Route exact path={config.routes.ORDER} component={NotFound} />
    <Route path={config.routes.ORDER_DETAIL + ":id"} component={NotFound} />
    <Route path={config.routes.PAYMENT_CB} component={NotFound} />
    <Route path="*" component={NotFound} />
  </Switch>
);
