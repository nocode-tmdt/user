import userApi from "~/apis/userApi";
import cartApi from "~/apis/cartApi";
import orderApi from "~/apis/orderApi";
import otherApi from "~/apis/otherApi";
import addressApi from "~/apis/addressApi";
import config from "~/constants";

export const authToken = (setIsLoading) => {
  return (dispatch) => {
    const token = localStorage.getItem("token");
    const logoutFunc = () => {
      dispatch(signOut());
      dispatch(deleteAllCart());
      dispatch(deleteAllOrder());
    };

    if (!token) {
      logoutFunc();
      if (setIsLoading !== undefined) {
        setIsLoading(true);
      }

      return false;
    }

    userApi
      .authToken()
      .then((res) => {
        if (res.status === config.response.SUCCESS) {
          dispatch(signIn(res.data));
          dispatch(getCart());
          // dispatch(getOrder());
          dispatch(getAddress());
        }
        if (setIsLoading !== undefined) {
          setIsLoading(true);
        }
      })
      .catch((err) => {
        logoutFunc();
        if (setIsLoading !== undefined) {
          setIsLoading(true);
        }
      });
  };
};

export const signIn = (data) => {
  return {
    type: config.types.SIGN_IN,
    payload: data,
  };
};

export const signOutReq = () => {
  return (dispatch) => {
    userApi
      .signOut()
      .then((res) => {
        if (res.status === config.response.SUCCESS) {
          dispatch(signOut());
          dispatch(deleteAllCart());
          dispatch(deleteAllOrder());
        }
      })
      .catch((err) => {});
  };
};

export const signOut = () => {
  return {
    type: config.types.SIGN_OUT,
  };
};

export const getCart = () => {
  return (dispatch) => {
    cartApi.getCart().then((res) => {
      if (res.status === config.response.SUCCESS) {
        dispatch(initCart(res.data));
      }
    });
  };
};

export const checkedCart = (value) => {
  return (dispatch) => {
    cartApi
      .checkedCart(value)
      .then((res) => {
        if (res.status === config.response.SUCCESS) {
          dispatch(getCart());
        }
      })
      .catch((err) => {});
  };
};

export const initCart = (values) => {
  return {
    type: config.types.INIT_CART,
    payload: values,
  };
};

export const deleteAllCart = () => {
  return {
    type: config.types.DELETE_ALL_CART,
  };
};

export const getOrder = () => {
  return (dispatch) => {
    orderApi
      .getOrderAll()
      .then((res) => {
        if (res.status === config.response.SUCCESS) {
          dispatch(initOrder(res.data));
        }
      })
      .catch((err) => {});
  };
};

export const getOrderByStatus = (status) => {
  return (dispatch) => {
    orderApi
      .getOrderByStatus(status)
      .then((res) => {
        if (res.status === config.response.SUCCESS) {
          dispatch(
            filterOrder({
              key: status,
              orders: res.data,
            })
          );
        }
      })
      .catch((err) => {});
  };
};

export const filterOrder = (values) => {
  return {
    type: config.types.FILTER_ORDER,
    payload: values,
  };
};

export const initOrder = (values) => {
  return {
    type: config.types.INIT_ORDER,
    payload: values,
  };
};

export const deleteAllOrder = () => {
  return {
    type: config.types.DELETE_ALL_ORDER,
  };
};

export const getAddress = () => {
  return (dispatch) => {
    addressApi
      .getAddress()
      .then((res) => {
        if (res.status === config.response.SUCCESS) {
          dispatch(initAddress(res.data));
        }
      })
      .catch((err) => {});
  };
};

export const initAddress = (values) => {
  return {
    type: config.types.INIT_ADDRESS,
    payload: values,
  };
};

export const getRssFeed = (key) => {
  return (dispatch) => {
    otherApi
      .getRssFeed(key)
      .then((res) => {
        const parser = new DOMParser();
        const xml = parser.parseFromString(res.data, "text/xml");

        const feed = {
          title: xml.querySelector("title").textContent,
          image: xml.querySelector("image").querySelector("url").textContent,
          url: xml.querySelector("image").querySelector("link").textContent,
          date: xml.querySelector("pubDate").textContent,
        };

        const items = [];
        for (let item of xml.querySelectorAll("item")) {
          const cdata = item.querySelector("description").textContent;
          const desc = parser.parseFromString(cdata, "text/html");
          const img = desc.querySelector("img")?.src || null;

          if (img) {
            const newItem = {
              img,
              title: item.querySelector("title").textContent,
              url: item.querySelector("link").textContent,
              desc: desc.querySelector("body").textContent,
              date: item.querySelector("pubDate").textContent,
            };

            items.push(newItem);
          }
        }

        const news = { feed, items };

        dispatch(updateTab({ key, news }));
      })
      .catch((err) => {
        console.log(err);
      });

    // const tmp = localStorage.getItem("xml_rss");
    // localStorage.setItem("xml_rss", res.data);
    // const parser = new DOMParser();
    // const xml = parser.parseFromString(tmp, "text/xml");

    // const feed = {
    //   title: xml.querySelector("title").textContent,
    //   image: xml.querySelector("image").querySelector("url").textContent,
    //   url: xml.querySelector("image").querySelector("link").textContent,
    //   date: xml.querySelector("pubDate").textContent,
    // };

    // const items = [];
    // for (let item of xml.querySelectorAll("item")) {
    //   const cdata = item.querySelector("description").textContent;
    //   const desc = parser.parseFromString(cdata, "text/html");
    //   const img = desc.querySelector("img")?.src || null;

    //   if (img) {
    //     const newItem = {
    //       img,
    //       title: item.querySelector("title").textContent,
    //       url: item.querySelector("link").textContent,
    //       desc: desc.querySelector("body").textContent,
    //       date: item.querySelector("pubDate").textContent,
    //     };

    //     items.push(newItem);
    //   }
    // }

    // const news = { feed, items };

    // dispatch(updateTab({ key, news }));
  };
};

export const updateTab = (values) => {
  return {
    type: config.types.UPDATE_TAB,
    payload: values,
  };
};
