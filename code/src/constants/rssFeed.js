const rssFeed = [
  {
    name: "Mới nhất",
    key: "tin-moi-nhat.rss",
  },
  {
    name: "Nổi bật",
    key: "tin-noi-bat.rss",
  },
  {
    name: "Xem nhiều",
    key: "tin-xem-nhieu.rss",
  },
  {
    name: "Thế giới",
    key: "the-gioi.rss",
  },
  {
    name: "Thời sự",
    key: "thoi-su.rss",
  },
  {
    name: "Kinh doanh",
    key: "kinh-doanh.rss",
  },
  {
    name: "Giải trí",
    key: "giai-tri.rss",
  },
  {
    name: "Thể thao",
    key: "the-thao.rss",
  },
  {
    name: "Pháp luật",
    key: "phap-luat.rss",
  },
  {
    name: "Giáo dục",
    key: "giao-duc.rss",
  },
  {
    name: "Sức khỏe",
    key: "suc-khoe.rss",
  },
  {
    name: "Gia đình",
    key: "gia-dinh.rss",
  },
  {
    name: "Du lịch",
    key: "du-lich.rss",
  },
  {
    name: "Cười",
    key: "cuoi.rss",
  },
];

export default rssFeed;
