import routes from "./routes";
import rssFeed from "./rssFeed";
import * as envConst from "./envConst";
import * as api from "./apiUrl";
import response from "./response";
import orderTab from "./orderTab";
import * as types from "./actionTypes";

const config = {
  routes,
  rssFeed,
  envConst,
  api,
  response,
  types,
  orderTab,
};

export default config;
