const routes = {
  HOME: "/",
  SEARCH: "/search",
  YOUTUBE_SEARCH: "/youtube-search",
  PRODUCT: "/product/",
  CART: "/cart",
  ORDER: "/order",
  MESSAGE: "/message",
  PROFILE: "/profile",
  ACTIVITY: "/activity",
  CONTACT: "/contact",
  NEWS: "/news",
  ORDER_DETAIL: "/order/detail/",
  PAYMENT_CB: "/order/callback/",
  CHECKOUT: "/checkout",
  SIGN_IN: "/signin",
  SIGN_IN_SOCIAL_CB: "/signin/callback/:social",
  SIGN_UP: "/signup",
  NOT_FOUND: "/404",
};

export default routes;
