import * as envConst from "./envConst";

export const BASE_URL = envConst.BASE_URL;
export const BASE_URL_NODE = envConst.BASE_URL_NODE;

export const BASE_URL_API = "api/";
export const BASE_URL_CSRF = "sanctum/csrf-cookie";
export const BASE_URL_IMAGE = BASE_URL + "admins/uploads/";

//image
export const AVATAR_IMAGE = BASE_URL_IMAGE + "avatars/";
export const SLIDER_IMAGE = BASE_URL_IMAGE + "sliders/";
export const PRODUCT_IMAGE = BASE_URL_IMAGE + "products/";
export const GALLERY_IMAGE = BASE_URL_IMAGE + "gallerys/";

// authentication
export const AUTH_TOKEN = BASE_URL_API + "auth-token";
export const SIGN_IN = BASE_URL_API + "signin";
export const SIGN_IN_SOCIAL = BASE_URL_API + "signin/redirect/";
export const SIGN_IN_SOCIAL_CB = BASE_URL_API + "signin/callback/";
export const SIGN_UP = BASE_URL_API + "signup";
export const SIGN_OUT = BASE_URL_API + "signout";

//user
export const UPDATE_PROFILE = BASE_URL_API + "user/update-profile";
export const UPDATE_AVATAR = BASE_URL_API + "user/update-avatar";
export const UPDATE_PASSWORD = BASE_URL_API + "user/update-password";

//product
export const GET_PRODUCT = BASE_URL_API + "product/";
export const PRODUCT_SEARCH = BASE_URL_API + "product/search";
export const PRODUCT_NEW = BASE_URL_API + "product/new";
export const PRODUCT_FEATHER = BASE_URL_API + "product/feather";
export const PRODUCT_BRAND = BASE_URL_API + "product/brand/";
export const PRODUCT_MORE = BASE_URL_API + "product/more";
export const UPDATE_VIEW = BASE_URL_API + "product/update/";

//slider
export const GET_SLIDER = BASE_URL_API + "slider";

//comment
export const GET_COMMENT = BASE_URL_API + "comment/get";
export const NEW_COMMENT = BASE_URL_API + "comment/new";
export const UPDATE_COMMENT = BASE_URL_API + "comment/update";
export const DELETE_COMMENT = BASE_URL_API + "comment/delete";
export const PROUDCT_COMMENT = BASE_URL_API + "comment/product";

//cart
export const GET_CART = BASE_URL_API + "cart/get";
export const GET_CART_CHECKED = BASE_URL_API + "cart/get-checked";
export const NEW_CART = BASE_URL_API + "cart/new";
export const CHECKED_CART = BASE_URL_API + "cart/checked";
export const DELETE_CART = BASE_URL_API + "cart/delete";

//coupon
export const USE_COUPON = BASE_URL_API + "use-coupon";

// brand
export const GET_BRAND = BASE_URL_API + "brand/get";

//order
export const GET_ORDER_ALL = BASE_URL_API + "order";
export const GET_ORDER_BY_STATUS = BASE_URL_API + "order/status";
export const GET_ORDER_DETAIL = BASE_URL_API + "order/detail/";
export const ORDER_CB = BASE_URL_API + "order/callback";
export const NEW_ORDER = BASE_URL_API + "order/new";
export const ORDER_MOMO = BASE_URL_API + "order/payment-momo";
export const ORDER_VNPAY = BASE_URL_API + "order/payment-vnpay";
export const UPDATE_STATUS_ORDER = BASE_URL_API + "order/update-status";
export const PRINT_ORDER = BASE_URL + "order/print/";

//export
export const EXPORT_PDF_ORDER = BASE_URL_API + "export/order/";

//activity
export const GET_ACTIVITY = BASE_URL_API + "activity/get-activity";

//notification
export const SAVE_TOKEN_NOTIFICATION = BASE_URL_API + "notification/save-token";

//message
export const ALL_MESSAGE = BASE_URL_API + "message/get-message";
export const NEW_MESSAGE = BASE_URL_API + "message/new-message";

//address
export const GET_ADDRESS = BASE_URL_API + "address/get";
export const CALC_FEESHIP = BASE_URL_API + "address/feeship";
export const GET_PROVINCE =
  "https://online-gateway.ghn.vn/shiip/public-api/master-data/province";
export const GET_DISTRICT =
  "https://online-gateway.ghn.vn/shiip/public-api/master-data/district";
export const GET_WARD =
  "https://online-gateway.ghn.vn/shiip/public-api/master-data/ward";
export const GET_AVAILABLE_SERVICES =
  "https://online-gateway.ghn.vn/shiip/public-api/v2/shipping-order/available-services";
export const GET_FEESHIP =
  "https://online-gateway.ghn.vn/shiip/public-api/v2/shipping-order/fee";
export const GET_ESTIMATE_TIME =
  "https://online-gateway.ghn.vn/shiip/public-api/v2/shipping-order/leadtime";

//orther
export const YOUTUBE_SEARCH = "https://www.googleapis.com/youtube/v3/search";
export const CONVERT_MONEY = `https://free.currconv.com/api/v7/convert?q=VND_USD&apiKey=${envConst.CURRENCY_CONVERTER_KEY}`;
export const RSS_FEED =
  "https://cors-anywhere.herokuapp.com/https://vnexpress.net/rss/";
export const FB_GRAPH = "https://graph.facebook.com";
