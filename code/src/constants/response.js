const response = {
  SUCCESS: "SUCCESS",
  FAIL: "FAIL",
  HAS_USED: "HAS_USED",
  NOT_FOUND: "NOT_FOUND",
  EMAIL_EXIST: "EMAIL_EXIST",
  SIGN_UP_SUCCESS: "SIGN_UP_SUCCESS",
};

export default response;
