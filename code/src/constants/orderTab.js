const orderTab = [
  {
    key: "all",
    name: "Tất cả",
  },
  {
    key: "awaiting_payment",
    name: "Chưa thanh toán",
  },
  {
    key: "processing",
    name: "Chờ xác nhận",
  },
  {
    key: "transporting",
    name: "Đang vận chuyển",
  },
  {
    key: "delivered",
    name: "Đã giao hàng",
  },
  {
    key: "canceled",
    name: "Đã hủy",
  },
];

export default orderTab;
