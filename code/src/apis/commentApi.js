import axiosClient from "./axiosClient";
import config from "~/constants";

const commentApi = {
  newUserComment: (value) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.NEW_COMMENT, value);
    });
  },
  getUserComment: (value) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.GET_COMMENT, value);
    });
  },
  getProductComment: (value) => {
    return axiosClient.post(config.api.PROUDCT_COMMENT, value);
  },
  updateUserComment: (value) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.UPDATE_COMMENT, value);
    });
  },
  deleteUserComment: (value) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.DELETE_COMMENT, value);
    });
  },
};

export default commentApi;
