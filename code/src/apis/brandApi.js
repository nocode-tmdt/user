import axiosClient from "./axiosClient";
import config from "~/constants";

const brandApi = {
  getAllBrand: () => {
    return axiosClient.get(config.api.GET_BRAND);
  },
};

export default brandApi;
