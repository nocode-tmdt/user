import axiosClient from "./axiosClient";
import config from "~/constants";

const userApi = {
  authToken: () => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.AUTH_TOKEN);
    });
  },
  signIn: (value) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.SIGN_IN, value);
    });
  },
  signInSocial: (social) => {
    return axiosClient.get(config.api.SIGN_IN_SOCIAL + social);
  },
  signInSocialCb: (social) => {
    return axiosClient.get(config.api.SIGN_IN_SOCIAL_CB + social);
  },
  signUp: (value) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.SIGN_UP, value);
    });
  },
  signOut: () => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.SIGN_OUT);
    });
  },
  updateProfile: (profile) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.UPDATE_PROFILE, profile);
    });
  },
  updateAvatar: (fmData, configReq) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.UPDATE_AVATAR, fmData, configReq);
    });
  },
  updatePassword: (password) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.UPDATE_PASSWORD, password);
    });
  },
};

export default userApi;
