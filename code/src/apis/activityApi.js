import axiosClient from "./axiosClient";
import config from "~/constants";

const activityApi = {
  getActivity: (params) => {
    return axiosClient.get(config.api.GET_ACTIVITY, { params });
  },
};

export default activityApi;
