import axiosClient from "./axiosClient";
import config from "~/constants";

const orderApi = {
  getOrderAll: () => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.get(config.api.GET_ORDER_ALL);
    });
  },
  getOrderByStatus: (status) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.get(config.api.GET_ORDER_BY_STATUS, {
        params: { status },
      });
    });
  },
  getOrderDetail: (id) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.get(config.api.GET_ORDER_DETAIL + id);
    });
  },
  newOrder: (value) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.NEW_ORDER, value);
    });
  },
  orderMomo: (value) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.ORDER_MOMO, value);
    });
  },
  orderVNPay: (value) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.ORDER_VNPAY, value);
    });
  },
  updateStatus: (value) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.UPDATE_STATUS_ORDER, value);
    });
  },
  orderCb: (value) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.get(config.api.ORDER_CB + value);
    });
  },
};

export default orderApi;
