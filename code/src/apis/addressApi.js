import axiosClient, { axiosInstance } from "./axiosClient";
import config from "~/constants";

axiosInstance.defaults.headers.common["token"] = config.envConst.GHN_TOKEN;

const addressApui = {
  getAddress: () => {
    return axiosClient.get(config.api.GET_ADDRESS);
  },
  getProvince: () => {
    return axiosInstance.post(config.api.GET_PROVINCE);
  },
  getDistrict: (province_id) => {
    return axiosInstance.post(config.api.GET_DISTRICT, { province_id });
  },
  getWard: (district_id) => {
    return axiosInstance.post(config.api.GET_WARD, { district_id });
  },
  getAvailableServices: (to_district) => {
    return axiosInstance.post(config.api.GET_AVAILABLE_SERVICES, {
      shop_id: 2355389,
      from_district: 1447,
      to_district,
    });
  },

  getFeeship: (data) => {
    const { service_id, insurance_value, to_district_id, to_ward_code } = data;
    return axiosInstance.post(config.api.GET_FEESHIP, {
      service_id,
      insurance_value,
      coupon: null,
      from_district_id: 1447,
      to_district_id,
      to_ward_code,
      height: 15,
      length: 15,
      weight: 500,
      width: 15,
    });
  },
  getEstimateTime: (data) => {
    axiosInstance.defaults.headers.common["ShopID"] = 2355389;
    const { service_id, to_district_id, to_ward_code } = data;

    return axiosInstance.post(config.api.GET_ESTIMATE_TIME, {
      from_district_id: 1447,
      from_ward_code: "20504",
      to_district_id,
      to_ward_code: to_ward_code.toString(),
      service_id,
    });
  },
  calcFeeship: (values) => {
    return axiosClient.post(config.api.CALC_FEESHIP, values);
  },
};

export default addressApui;
