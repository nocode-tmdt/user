import axiosClient from "./axiosClient";
import config from "~/constants";

const cartApi = {
  getCart: () => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.get(config.api.GET_CART);
    });
  },
  getCartChecked: () => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.get(config.api.GET_CART_CHECKED);
    });
  },
  newCart: (value) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.NEW_CART, value);
    });
  },
  checkedCart: (value) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.CHECKED_CART, value);
    });
  },
  deleteCart: (value) => {
    return axiosClient.get(config.api.BASE_URL_CSRF).then((res) => {
      return axiosClient.post(config.api.DELETE_CART, value);
    });
  },
};

export default cartApi;
