import { useEffect } from "react";
import { initFacebookSDK } from "~/services/facebookService";

function useFacebookSDK() {
  useEffect(() => {
    initFacebookSDK();
  }, []);
}

export default useFacebookSDK;
