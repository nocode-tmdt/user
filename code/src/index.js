import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import App from "~/App";
import store from "~/store/store";
import { I18nextProvider } from "react-i18next";
import i18n from "~/translation/i18n";
import reportWebVitals from "~/reportWebVitals";

ReactDOM.render(
  <Provider store={store}>
    <I18nextProvider i18n={i18n}>
      <App />
    </I18nextProvider>
  </Provider>,
  document.getElementById("root")
);

reportWebVitals();
