import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { authToken } from "~/actions/action";
import MainLayout from "./MainLayout";
import AuthLoading from "~/components/Loading/AuthLoading";

function AuthLayout() {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    dispatch(authToken(setIsLoading));
  }, [dispatch]);

  if (!isLoading) {
    return <AuthLoading />;
  }

  return <MainLayout />;
}

export default AuthLayout;
