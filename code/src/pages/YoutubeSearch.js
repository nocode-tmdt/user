import React, { useState } from "react";
import moment from "moment";
import useTitle from "~/hooks/useTitle";
import otherApi from "~/apis/otherApi";

function YoutubeSearch() {
  const [videos, setVideos] = useState([]);
  const [filters, setFilters] = useState("");

  useTitle("Tìm kiếm Youtube");

  const handleChangeKeyword = (e) => {
    setFilters(e.target.value);
  };

  const handleSearch = () => {
    if (filters === "") {
      setVideos([]);
    } else {
      otherApi
        .youtubeSearch({
          part: "snippet",
          maxResults: "20",
          type: "video",
          key: process.env.REACT_APP_YOUTUBE_KEY,
          q: filters,
        })
        .then((res) => {
          const videos = res.items.map((video) => {
            const {
              title: name,
              publishedAt: time,
              channelTitle: creator,
              description: desc,
            } = video.snippet;

            return {
              id: video.id.videoId,
              name,
              time,
              creator,
              desc,
            };
          });

          setVideos(videos);
        })
        .catch((err) => {});
    }
  };

  return (
    <div className="search-page">
      <h3 className="title-section">Tìm kiếm youtube</h3>
      <div className="row justify-content-center">
        <div className="col-12 col-md-12 col-lg-6 col-xl-6">
          <div className="search-form">
            <div className="input-field">
              <input
                type="text"
                name="keyword"
                onChange={handleChangeKeyword}
                className="form-control"
                placeholder="Nhập từ khóa để tìm kiếm..."
                onKeyDown={(e) => (e.key === "Enter" ? handleSearch() : null)}
              />
              <i className="fas fa-search" onClick={handleSearch} />
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        {videos.map((video, key) => (
          <div key={key} className="youtube-result">
            <div className="col-5">
              <div className="video">
                <iframe
                  width="450"
                  height="270"
                  src={`https://www.youtube.com/embed/${video.id}`}
                  title={video.name}
                  frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen
                ></iframe>
              </div>
            </div>
            <div className="col-7">
              <div className="video-info">
                <a
                  href={`https://www.youtube.com/watch?v=${video.id}`}
                  className="name"
                  target="_blank"
                  rel="noreferrer"
                >
                  {video.name}
                </a>
                <p className="time">
                  Time: {moment(video.time).format("DD-MM-YY H:mm:ss")}
                </p>
                <p className="creator">Autor: {video.creator}</p>
                <p className="desc">{video.desc}</p>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default YoutubeSearch;
