import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useLocation, useHistory } from "react-router-dom";
import io from "socket.io-client";
import useTitle from "~/hooks/useTitle";
import { getCart } from "../actions/action";
import toast from "~/helpers/toast";
import config from "~/constants";
import orderApi from "~/apis/orderApi";
import WaveLoading from "~/components/Loading/WaveLoading";

import { Button, Result } from "antd";

const socket = io(config.api.BASE_URL_NODE);

function Payment() {
  const dispatch = useDispatch();
  const location = useLocation();
  const history = useHistory();
  const user = useSelector((state) => state.userReducer.user);
  const [isLoading, setIsLoading] = useState(true);
  const [isSuccess, setIsSuccess] = useState(false);

  useTitle("Thanh toán trực tuyến");

  useEffect(() => {
    orderApi
      .orderCb(location.search)
      .then((res) => {
        if (res.status === config.response.SUCCESS) {
          console.log(res);
          dispatch(getCart());
          toast.success("Thành công", "Mua hàng thành công");
          setIsLoading(false);
          setIsSuccess(true);

          socket.emit("send_notification", {
            notification: `${user.name} has just added one more order`,
          });
        } else {
          setIsLoading(false);
        }
      })
      .catch((err) => {
        setIsLoading(false);
      });
  }, [dispatch, location.search, user.name]);

  const handleClick = (route) => {
    history.push(route);
  };

  return (
    <div>
      {isLoading ? (
        <>
          <WaveLoading />
        </>
      ) : (
        <>
          {isSuccess ? (
            <Result
              status="success"
              title="Thành công"
              subTitle="Đơn hàng đã được thanh toán thành công."
              extra={[
                <Button
                  type="primary"
                  key="console"
                  onClick={() => handleClick(config.routes.ORDER)}
                >
                  Đơn hàng
                </Button>,
                <Button
                  key="buy"
                  onClick={() => handleClick(config.routes.HOME)}
                >
                  Trang chủ
                </Button>,
              ]}
            />
          ) : (
            <Result
              status="error"
              title="Thất bại"
              subTitle="Đơn hàng chưa được thanh toán vui lòng thử lại."
              extra={[
                <Button
                  type="primary"
                  key="console"
                  onClick={() => handleClick(config.routes.ORDER)}
                >
                  Đơn hàng
                </Button>,
                <Button
                  key="buy"
                  onClick={() => handleClick(config.routes.HOME)}
                >
                  Trang chủ
                </Button>,
              ]}
            />
          )}
        </>
      )}
    </div>
  );
}

export default Payment;
