import React from "react";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import useTitle from "~/hooks/useTitle";
import useToggle from "~/hooks/useToggle";
import ModalAvatar from "~/components/Profile/ModalAvatar";
import ModalPassword from "~/components/Profile/ModalPassword";
import ModalProfile from "~/components/Profile/ModalProfile";
import InfoItem from "~/components/Profile/InfoItem";
import config from "~/constants";
import coverImg from "~/assets/img/cover/cover.jpg";

function Profile() {
  const { t } = useTranslation();
  const user = useSelector((state) => state.userReducer.user);
  const [openInfo, toggleInfo] = useToggle(false);
  const [openAvatar, toggleAvatar] = useToggle(false);
  const [openPassword, togglePassword] = useToggle(false);

  useTitle(t("profile.user_information"));

  return (
    <div className="profile-page">
      <h3 className="title-section">{t("profile.user_information")}</h3>
      <div className="row justify-content-center">
        <div className="profile">
          <div className="profile-header">
            <div className="header-background">
              <img src={coverImg} alt="cover" />
            </div>
            <div className="header-info">
              <div className="avatar">
                <img
                  src={config.api.AVATAR_IMAGE + user.image}
                  alt={user.name}
                />
                <div className="edit-avatar" onClick={toggleAvatar}>
                  <i className="fas fa-pen"></i>
                </div>
              </div>
              <h3 className="name">{user.name}</h3>
            </div>
          </div>
          <div className="profile-body">
            <InfoItem
              title={t("common.name")}
              content={user.name}
              icon={<i className="fas fa-user"></i>}
            />
            <InfoItem
              title={t("common.email")}
              content={user.email}
              icon={<i className="fas fa-envelope"></i>}
            />
            <InfoItem
              title={t("common.phone")}
              content={user.phone}
              icon={<i className="fas fa-mobile"></i>}
            />
            <InfoItem
              title={t("common.status")}
              content={user.status}
              icon={<i className="fas fa-info-circle"></i>}
            />
            <InfoItem
              title={t("common.address")}
              content={user.address}
              icon={<i className="fas fa-map-marked-alt"></i>}
            />

            <div className="info-item">
              <div className="info-icon">
                <i className="fas fa-map-marked-alt"></i>
              </div>
              <div className="info-left">
                <p className="info-title">{t("common.password")}</p>
                <h4 className="info-content">*********</h4>
              </div>
              <div className="info-icon" onClick={togglePassword}>
                <i className="fas fa-edit"></i>
              </div>
            </div>
          </div>
          <div className="profile-edit">
            <div className="btn edit-info" onClick={toggleInfo}>
              {t("profile.edit_information")}
            </div>
          </div>
        </div>
      </div>
      <ModalAvatar openAvatar={openAvatar} toggleAvatar={toggleAvatar} />
      <ModalPassword
        openPassword={openPassword}
        togglePassword={togglePassword}
      />
      <ModalProfile user={user} openInfo={openInfo} toggleInfo={toggleInfo} />
    </div>
  );
}

export default Profile;
