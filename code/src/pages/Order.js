import React, { useState, useEffect, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Tabs } from "antd";
import { useTranslation } from "react-i18next";
import config from "~/constants";
import { getOrderByStatus } from "~/actions/action";
import useTitle from "~/hooks/useTitle";
import OrderList from "~/components/Order/OrderList";

const orderTab = config.orderTab;

const { TabPane } = Tabs;

function Order() {
  const dispatch = useDispatch();
  const orders = useSelector((state) => state.orderReducer);
  const { t } = useTranslation();
  const [currentTab, setCurrentTab] = useState({
    key: orderTab[0].key,
  });
  const visited = useRef({
    tabs: [],
  });

  useTitle(t("header.order"));

  useEffect(() => {
    const { key } = currentTab;

    const isVisitedTab = visited.current.tabs.includes(key);

    const isFetch = !isVisitedTab;

    if (isFetch) {
      visited.current = {
        ...visited.current,
        tabs: !isVisitedTab
          ? [...visited.current.tabs, key]
          : [...visited.current.tabs],
      };
      const status = key;

      dispatch(getOrderByStatus(status));
    }
  }, [currentTab]);

  const onChange = (key) => {
    setCurrentTab({
      ...currentTab,
      key,
    });
  };

  return (
    <>
      <h3 className="title-section">{t("header.order")}</h3>
      <div className="cart-section row">
        <div className="col-12 col-md-12 col-lg-12 col-xl-12">
          <Tabs defaultActiveKey={currentTab} onChange={onChange}>
            {orderTab.map((item) => {
              const { key, name } = item;

              return (
                <TabPane tab={name} key={key} className="content-desciption">
                  {orders[key] && <OrderList orders={orders[key].orders} />}
                </TabPane>
              );
            })}
          </Tabs>
        </div>
      </div>
    </>
  );
}

export default Order;
