import React, { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { useTranslation } from "react-i18next";
import { Helmet } from "react-helmet";
import sliderApi from "~/apis/sliderApi";
import productApi from "~/apis/productApi";
import Slider from "~/components/Slider";
import ProductList from "~/components/Product/ProductList";
import WaveLoading from "~/components/Loading/WaveLoading";
import logoIcon from "~/assets/img/logo/logo.jpg";

function Home() {
  const { t } = useTranslation();
  const [sliders, setSliders] = useState([]);
  const [productNews, setProductNews] = useState([]);
  const [productFeathers, setProductFeathers] = useState([]);
  const [productMore, setProductMore] = useState([]);
  const [noMore, setNoMore] = useState(true);
  const [pageLoad, setPageLoad] = useState({ page: 1, lastPage: 1 });

  useEffect(() => {
    const fetchProduct = async () => {
      try {
        const res = await sliderApi.getAll();
        setSliders(res.data);
      } catch (error) {}

      try {
        const res = await productApi.getProductFeather();
        setProductFeathers(res.data);
      } catch (error) {}

      try {
        const res = await productApi.getProductNew();
        setProductNews(res.data);
      } catch (error) {}
    };

    fetchProduct();
  }, []);

  const fetchProductMore = async () => {
    const { page, lastPage } = pageLoad;
    if (page <= lastPage) {
      try {
        const res = await productApi.getProductMore({ page });
        const products = res.data;

        if (page <= products.last_page) {
          setPageLoad({ page: page + 1, lastPage: products.last_page });
        }
        setProductMore([...productMore, ...products.data]);
      } catch (error) {}
    } else {
      setNoMore(false);
    }
  };

  return (
    <>
      <Helmet>
        <title>MW Store - Website thương mại điện tử số 1 Việt Nam</title>
        <link rel="canonical" href="https://hstore.pw" />
        <meta
          name="keywords"
          content="MW Store, dtdd, smartphone, tablet, máy tính bảng, laptop, máy tính xách tay, phụ kiện, smartwatch, đồng hồ, tin công nghệ"
        />
        <meta
          name="description"
          content="Website bán lẻ điện thoại di động, smartphone, máy tính bảng, tablet, laptop, phụ kiện, smartwatch, đồng hồ chính hãng mới nhất, giá tốt, dịch vụ khách hàng được yêu thích nhất VN"
        />
        <meta property="og:url" content="https://hstore.pw" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="MW Store - Website thương mại điện tử số 1 Việt Nam"
        />
        <meta
          property="og:description"
          content="Website bán lẻ điện thoại di động, smartphone, máy tính bảng, tablet, laptop, phụ kiện, smartwatch, đồng hồ chính hãng mới nhất, giá tốt, dịch vụ khách hàng được yêu thích nhất VN"
        />
        <meta property="og:image" content={logoIcon} />
      </Helmet>

      {sliders && sliders.length >= 1 && <Slider sliders={sliders} />}
      <h2 className="title-section">{t("product.product_feather")}</h2>
      <ProductList products={productFeathers} />
      <h2 className="title-section">{t("product.product_new")}</h2>
      <ProductList products={productNews} />
      <h2 className="title-section">{t("product.product_more")}</h2>
      <InfiniteScroll
        style={{ overflow: "visible" }}
        dataLength={productMore.length}
        next={fetchProductMore}
        hasMore={noMore}
        loader={<WaveLoading />}
        endMessage={
          <p className="no-item">{t("product.all_products_displayed")}</p>
        }
      >
        <ProductList products={productMore} />
      </InfiniteScroll>
    </>
  );
}

export default Home;
