import React, { useState, useEffect, useRef } from "react";
import { useParams, Redirect } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { Button } from "antd";
import { DownloadOutlined } from "@ant-design/icons";
import { isEmpty } from "lodash";
import Pdf from "react-to-pdf";
import useTitle from "~/hooks/useTitle";
import useDownload from "~/hooks/useDownload";
import orderApi from "~/apis/orderApi";
import config from "~/constants";
import { formatPrice, formatPhone } from "~/helpers/helpers";
import WaveLoading from "~/components/Loading/WaveLoading";

function OrderDetail() {
  const { id } = useParams();
  const { t } = useTranslation();
  const [order, setOrder] = useState({});
  const [user, setUser] = useState({});
  const [shipping, setShipping] = useState({});
  const [products, setProducts] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const [productPrice, setProductPrice] = useState(0);
  const [couponPrice, setCouponPrice] = useState(0);
  const [feeshipPrice, setFeeshipPrice] = useState(25000);
  const [redirectOrder, setRedirectOrder] = useState(false);
  const [keyOrder, setKeyOrder] = useState("");

  const refPdf = useRef();

  const [downloadFile, isDownloading, initFetch] = useDownload(
    keyOrder,
    "order_detail",
    "pdf"
  );

  useTitle(t("header.order_detail"));

  useEffect(() => {
    orderApi
      .getOrderDetail(id)
      .then((res) => {
        if (res.status === config.response.SUCCESS) {
          const order = res.data;

          const {
            user,
            shipping,
            product_money,
            total_money,
            coupon_money,
            feeship_money,
            order_details: products,
          } = order;


          setTotalPrice(parseInt(total_money));
          setOrder(order);
          setUser(user);
          setCouponPrice(parseInt(coupon_money));
          setFeeshipPrice(parseInt(feeship_money));
          setProductPrice(parseInt(product_money));
          setShipping(shipping);
          setProducts(products);

          setKeyOrder(
            config.api.EXPORT_PDF_ORDER +
              new Buffer(
                `${order.id}--${order.code}--${order.user_id}--${order.time}`
              ).toString("base64")
          );
        } else {
          setRedirectOrder(true);
        }
      })
      .catch((err) => {});
  }, [id]);

  if (redirectOrder) {
    return <Redirect to={config.routes.ORDER} />;
  }

  return (
    <>
      {!isEmpty(order) && !isEmpty(shipping) ? (
        <div className="order-detail-section" ref={refPdf}>
          <h3 className="title-section">{t("header.order_detail")}</h3>

          <div className="detail-part first-part">
            <h4 className="detail-title">{t("order.customer_information")}</h4>
            <div className="row">
              <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                <table className="table">
                  <thead>
                    <tr>
                      <th>{t("common.name")}</th>
                      <th>{t("common.email")}</th>
                      <th>{t("common.phone")}</th>
                      <th>{t("common.address")}</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>{user.name}</td>
                      <td>{user.email}</td>
                      <td>{formatPhone(user.phone || "")}</td>
                      <td>
                        {user.address !== null
                          ? order.user.address
                          : "Chưa cập nhập"}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div className="detail-part">
            <h4 className="detail-title">{t("order.recipient_information")}</h4>
            <div className="row">
              <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                <table className="table">
                  <thead>
                    <tr>
                      <th>{t("common.name")}</th>
                      <th>{t("common.phone")}</th>
                      <th>{t("common.payment")}</th>
                      <th>{t("common.note")}</th>
                      <th>{t("common.address")}</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>{shipping.name}</td>
                      <td>{formatPhone(shipping.phone || "")}</td>
                      <td>
                        {parseInt(shipping.method) === 0
                          ? "Tiền mặt"
                          : parseInt(shipping.method) === 1
                          ? "VN Pay"
                          : parseInt(shipping.method) === 2
                          ? "Momo"
                          : "Paypal"}
                      </td>
                      <td>
                        {shipping.note !== null
                          ? shipping.note
                          : "Không có ghi chú"}
                      </td>
                      <td>{shipping.address}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div className="detail-part">
            <h4 className="detail-title">{t("order.products_ordered")}</h4>
            <div className="row">
              <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                <table className="table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>{t("common.name")}</th>
                      <th>{t("common.price")}</th>
                      <th>{t("common.quantity")}</th>
                      <th>{t("common.total_money")}</th>
                    </tr>
                  </thead>
                  <tbody>
                    {products.map((product, index) => (
                      <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{product.product_name}</td>
                        <td>{formatPrice(product.product_price)}</td>
                        <td>{product.product_quantity}</td>
                        <td>{formatPrice(product.total_price)}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div className="detail-part">
            <h4 className="detail-title">{t("order.billing_information")}</h4>
            <div className="row">
              <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                <table className="table">
                  <thead>
                    <tr>
                      <th>{t("order.temporary_price")}</th>
                      <th>{t("product.discount")}</th>
                      <th>{t("order.feeship")}</th>
                      <th>{t("common.total_money")}</th>
                      <th>{t("order.print_invoice")}</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>{formatPrice(productPrice)}</td>
                      <td>{formatPrice(couponPrice)}</td>
                      <td>{formatPrice(feeshipPrice)}</td>
                      <td>{formatPrice(totalPrice)}</td>
                      <td>
                        <Pdf
                          targetRef={refPdf}
                          options={{
                            orientation: "landscape",
                            unit: "px",
                          }}
                          filename="order.pdf"
                        >
                          {({ toPdf }) => (
                            <Button
                              type="primary"
                              shape="round"
                              size="large"
                              onClick={toPdf}
                              icon={<DownloadOutlined />}
                            >
                              Print
                            </Button>
                          )}
                        </Pdf>
                        <Button
                          type="primary"
                          shape="round"
                          size="large"
                          disabled={initFetch}
                          loading={isDownloading}
                          icon={<DownloadOutlined />}
                          onClick={downloadFile}
                        >
                          Export PDF
                        </Button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <WaveLoading />
      )}
    </>
  );
}

export default OrderDetail;
