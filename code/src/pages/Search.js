import React, { useRef, useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Select, Slider } from "antd";
import className from "classnames/bind";
import useSpeechToText from "react-hook-speech-to-text";
import useTitle from "~/hooks/useTitle";
import config from "~/constants";
import brandApi from "~/apis/brandApi";
import productApi from "~/apis/productApi";
import ProductList from "~/components/Product/ProductList";

const cx = className;

const { Option } = Select;
const marks = {
  0: "0 tr",
  100: {
    style: {
      color: "#f50",
    },
    label: <strong>100tr</strong>,
  },
};

function formatter(value) {
  return `${value} tr`;
}

function Search() {
  const [oneVisit, setOneViset] = useState(true);
  const [brands, setBrands] = useState([]);
  const [products, setProducts] = useState([]);
  const [valueSearch, setValueSearch] = useState("");
  const { t } = useTranslation();
  const [filters, setFilters] = useState({
    keyword: "",
    brand: "",
    price: "",
    view: "",
    min_price: 1000000,
    max_price: 50000000,
  });
  const typingTimeoutRef = useRef(null);

  const {
    error,
    interimResult,
    isRecording,
    results,
    startSpeechToText,
    stopSpeechToText,
  } = useSpeechToText({
    continuous: true,
    useLegacyResults: false,
  });

  useTitle(t("header.search"));

  useEffect(() => {
    brandApi
      .getAllBrand()
      .then((res) => {
        if (res.status === config.response.SUCCESS) {
          setBrands(res.data);
        }
      })
      .catch((err) => {});
  }, []);

  useEffect(() => {
    if (filters.keyword === "") {
      setProducts([]);
    } else {
      productApi
        .getProductSearch(filters)
        .then((res) => {
          // console.log(res);
          if (res.status === config.response.SUCCESS) {
            setProducts(res.data);
          }
        })
        .catch((err) => {});
      setOneViset(false);
    }
  }, [filters]);

  const handleChangeKeyword = (e) => {
    setValueSearch(e.target.value);
    if (typingTimeoutRef.current) {
      clearTimeout(typingTimeoutRef.current);
    }

    typingTimeoutRef.current = setTimeout(() => {
      setFilters({ ...filters, keyword: e.target.value });
    }, 400);
  };

  function handleChangeBrand(brand) {
    setFilters({ ...filters, brand });
  }
  function handleChangePrice(price) {
    setFilters({ ...filters, price });
  }
  function handleChangeView(view) {
    setFilters({ ...filters, view });
  }

  function onChangeSeekbar(value) {
    if (typingTimeoutRef.current) {
      clearTimeout(typingTimeoutRef.current);
    }

    typingTimeoutRef.current = setTimeout(() => {
      const min_price = value[0] * 1000000;
      const max_price = value[1] * 1000000;

      setFilters({ ...filters, min_price, max_price });
    }, 500);
  }
  const handleStopRecord = () => {
    stopSpeechToText();

    const keyword = results[results.length - 1]?.transcript;
    if (keyword) {
      console.log("Text :", results, keyword);
      setValueSearch(keyword);
      setFilters({ ...filters, keyword });
    }
  };

  return (
    <div className="search-page">
      <h3 className="title-section">{t("header.search")}</h3>
      <div className="row justify-content-center">
        <div className="col-12 col-md-12 col-lg-6 col-xl-6">
          <form className="search-form">
            <div className="input-field">
              <input
                type="text"
                name="keyword"
                value={valueSearch}
                onChange={handleChangeKeyword}
                className="form-control"
                placeholder={t("search.please_input_keyword")}
              />
              <div
                onClick={isRecording ? handleStopRecord : startSpeechToText}
                className={cx("search-icon", "microphone", {
                  red: isRecording,
                })}
              >
                <i className="fas fa-microphone" />
              </div>
              <div className="search-icon search">
                <i className="fas fa-search" />
              </div>
            </div>
          </form>
        </div>
      </div>
      <div className="row">
        <div className="search-filter">
          <div className="row">
            <div className="col-6 col-md-6 col-lg-3 col-xl-3">
              <div className="filter-item">
                <Select
                  onChange={handleChangeBrand}
                  placeholder={t("common.brand")}
                >
                  <Option value="">{t("common.all")}</Option>
                  {brands.map((brand) => (
                    <Option key={brand.id} value={brand.id}>
                      {brand.name}
                    </Option>
                  ))}
                </Select>
              </div>
            </div>
            <div className="col-6 col-md-6 col-lg-3 col-xl-3">
              <div className="filter-item">
                <Select
                  onChange={handleChangePrice}
                  placeholder={t("common.price")}
                >
                  <Option value="ASC">{t("search.price_goes_up")}</Option>
                  <Option value="DESC">{t("search.price_goes_down")}</Option>
                </Select>
              </div>
            </div>
            <div className="col-6 col-md-6 col-lg-3 col-xl-3">
              <div className="filter-item">
                <Select
                  onChange={handleChangeView}
                  placeholder={t("common.view")}
                >
                  <Option value="ASC">
                    {t("search.views_are_increasing")}
                  </Option>
                  <Option value="DESC">
                    {t("search.views_are_decreasing")}
                  </Option>
                </Select>
              </div>
            </div>
            <div className="col-6 col-md-6 col-lg-3 col-xl-3">
              <div className="filter-item">
                <Slider
                  range
                  marks={marks}
                  defaultValue={[0, 50]}
                  onChange={onChangeSeekbar}
                  tipFormatter={formatter}
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-12 col-md-12 col-lg-12 col-xl-12">
          <div className="product-result">
            {products.length ? (
              <>
                <h4 className="title-result">
                  {t("search.search_results_for")} <b>{filters.keyword}</b>
                </h4>
                <ProductList products={products} />
              </>
            ) : (
              !oneVisit && (
                <h4 className="not-product">{t("search.no_products_found")}</h4>
              )
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Search;
