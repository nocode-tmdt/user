import React, { useState } from "react";
import { Link, Redirect } from "react-router-dom";
import { useFormik } from "formik";
import { useTranslation } from "react-i18next";
import useTitle from "~/hooks/useTitle";
import userApi from "~/apis/userApi";
import config from "~/constants";
import toast from "~/helpers/toast";
import { signUpValid } from "~/helpers/validate";
import MWStoreLogo from "~/assets/img/logo/mwstore.png";
import WaveLoading from "~/components/Loading/WaveLoading";
import LoginImg from "~/assets/img/login/login.jpg";

function SignUp() {
  const { t } = useTranslation();
  const [pressSignUp, setPressSignUp] = useState(false);
  const [signupFail, setSignupFail] = useState(false);
  const [signupSuccess, setSignupSuccess] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  useTitle(t("common.sign_up"));

  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      password: "",
      pre_password: "",
    },
    validationSchema: signUpValid,
    onSubmit: (values) => {
      setPressSignUp(true);
      userApi
        .signUp(values)
        .then((res) => {
          if (res.status === config.response.SIGN_UP_SUCCESS) {
            setPressSignUp(false);
            setSignupSuccess(true);
          }

          if (res.message === config.response.EMAIL_EXIST) {
            setSignupFail(true);
            setPressSignUp(false);
            formik.setFieldError("email", t("common.email_used"));
          }

          if (res.status === config.response.FAIL) {
            setSignupFail(true);
            setPressSignUp(false);
            setErrorMessage(t("common.sign_up_fail"));
          }
        })
        .catch((err) => {
          setSignupFail(true);
          setPressSignUp(false);
          setErrorMessage(t("common.sign_up_fail"));
        });
    },
  });

  const focusInput = () => {
    setSignupFail(false);
    setErrorMessage("");
  };

  if (signupSuccess) {
    toast.success(t("common.success"), t("common.sign_up_success"));
    return <Redirect to={config.routes.SIGN_IN} />;
  }

  return (
    <div className="login-section row">
      <div className="col-12 col-md-6 col-lg-6 col-xl-6">
        <div className="panel-left">
          <div className="login-img">
            <img src={LoginImg} alt="" />
          </div>
          <h3 className="has-account">{t("common.has_account")}</h3>
          <Link to={config.routes.SIGN_IN} className="btn-change">
            {t("common.sign_in")}
          </Link>
        </div>
      </div>
      <div className="col-12 col-md-6 col-lg-6 col-xl-6">
        <div className="panel-right">
          <div className="logo-img">
            <img src={MWStoreLogo} alt="" />
          </div>
          <h2 className="title">{t("common.sign_up")}</h2>
          <form
            onSubmit={formik.handleSubmit}
            className="form-input form-signup"
          >
            <div className="form-group">
              <div className="input-field">
                <i className="fas fa-user" />
                <input
                  type="text"
                  name="name"
                  className="form-control"
                  value={formik.values.name}
                  onFocus={focusInput}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  placeholder={t("common.input_name")}
                />
              </div>
              {formik.errors.name && formik.touched.name && (
                <p className="error">{formik.errors.name}</p>
              )}
            </div>
            <div className="form-group">
              <div className="input-field">
                <i className="fas fa-envelope" />
                <input
                  type="text"
                  name="email"
                  className="form-control"
                  value={formik.values.email}
                  onFocus={focusInput}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  placeholder={t("common.input_email")}
                />
              </div>
              {formik.errors.email && formik.touched.email && (
                <p className="error">{formik.errors.email}</p>
              )}
            </div>
            <div className="form-group">
              <div className="input-field">
                <i className="fas fa-key" />
                <input
                  type="password"
                  name="password"
                  className="form-control"
                  value={formik.values.password}
                  onFocus={focusInput}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  placeholder={t("common.input_password")}
                />
              </div>
              {formik.errors.password && formik.touched.password && (
                <p className="error">{formik.errors.password}</p>
              )}
            </div>

            <div className="form-group">
              <div className="input-field">
                <i className="fas fa-key" />
                <input
                  type="password"
                  name="pre_password"
                  className="form-control"
                  value={formik.values.pre_password}
                  onFocus={focusInput}
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  placeholder={t("common.input_re_password")}
                />
              </div>
              {formik.errors.pre_password && formik.touched.pre_password && (
                <p className="error">{formik.errors.pre_password}</p>
              )}
            </div>

            <div className="login-action">
              {signupFail && <p className="login-fail">{errorMessage}</p>}
              {pressSignUp && (
                <div style={{ marginTop: "10px" }}>
                  <WaveLoading />
                </div>
              )}
              <input
                type="submit"
                disabled={pressSignUp}
                className="btn-submit"
                value={t("common.sign_up")}
              />
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SignUp;
