import React from "react";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import { Empty } from "antd";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import useTitle from "~/hooks/useTitle";
import toast from "~/helpers/toast";
import config from "~/constants";
import { formatPrice } from "~/helpers/helpers";
import CartList from "~/components/Cart/CartList";

function Cart() {
  const history = useHistory();
  const { t } = useTranslation();
  const carts = useSelector((state) => state.cartReducer.carts);
  const totalPrice = useSelector((state) => state.cartReducer.totalPrice);

  useTitle(t("header.cart"));

  const handleCheckout = () => {
    const index = carts.findIndex((cart) => cart.checked);
    if (index === -1) {
      return toast.error(t("common.fail"), t("cart.choose_a_product_to_buy"));
    }
    return history.push(config.routes.CHECKOUT);
  };

  return (
    <>
      <h3 className="title-section">{t("header.cart")}</h3>
      {carts && carts.length > 0 && (
        <div className="cart-section row">
          <div className="col-12 col-md-12 col-lg-12 col-xl-12">
            <CartList />
          </div>
          <div className="cart-total">
            <div className="total-money-wrapper">
              <h3 className="title-money">{t("common.total_money")}: </h3>
              <p className="total-money"> {formatPrice(totalPrice)}</p>
            </div>
            <div className="btn btn-checkout" onClick={handleCheckout}>
              {t("cart.purchase")}
            </div>
          </div>
        </div>
      )}
      {carts && carts.length <= 0 && (
        <Empty
          image={Empty.PRESENTED_IMAGE_SIMPLE}
          description={
            <span>{t("cart.no_products_in_the_shopping_cart")}</span>
          }
        ></Empty>
      )}
      {!carts && (
        <Skeleton
          count={5}
          height={"30px"}
          duration={1}
          style={{ margin: "6px 0" }}
        />
      )}
    </>
  );
}

export default Cart;
